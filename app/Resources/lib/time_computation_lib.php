<?php

function time_elapsed($secs){
  $ret = array();
  $bit = array(
    ' annee'        => $secs / 31556926 % 12,
    ' semaine'        => $secs / 604800 % 52,
    ' jour'        => $secs / 86400 % 7,
    ' heure'        => $secs / 3600 % 24,
    ' minute'    => $secs / 60 % 60,
    ' seconde'    => $secs % 60
  );
       
  foreach($bit as $k => $v){
    if($v > 1)$ret[] = $v . $k . 's';
    if($v == 1)$ret[] = $v . $k;
  }
  if(count($ret) == 0){
    $ret[]= '0 seconde';
  }
  array_splice($ret, count($ret)-1, 0, '');
   
  return join(' ', $ret);
}