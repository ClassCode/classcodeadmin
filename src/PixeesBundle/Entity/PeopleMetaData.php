<?php

namespace PixeesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * PixeesBundle\Entity\PeopleMetaData
 *
 * @ORM\Table(name="wp_usermeta")
 * @ORM\Entity(repositoryClass="PixeesBundle\Entity\PeopleMetaDataRepository")
 */
class PeopleMetaData
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="umeta_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer $userId
     *
     * @ORM\ManyToOne(targetEntity="People", inversedBy="metaDatas")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @var string $key
     *
     * @ORM\Column(name="meta_key", type="string", length=255)
     * 
     *  
     */
    private $key;
    
    /**
     * @var string $value
     *
     * @ORM\Column(name="meta_value", type="string", length=255)
     * 
     *  
     */
    private $value;

    public function __construct() {

    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Get key
     *
     * @return string 
     */
    public function getKey()
    {
      return $this->key;
    }
    
    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
      return $this->value;
    }
    
}