<?php

namespace PixeesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * PixeesBundle\Entity\People
 *
 * @ORM\Table(name="wp_users")
 * @ORM\Entity(repositoryClass="PixeesBundle\Entity\PeopleRepository")
 */
class People implements UserInterface
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $username
     *
     * @ORM\Column(name="user_login", type="string", length=255)
     * 
     *  
     */
    private $username;
    
    /**
     * @var string $displayname
     *
     * @ORM\Column(name="user_nicename", type="string", length=255)
     * 
     *  
     */
    private $displayname;
    
    
    /**
     * @var string $password
     *
     * @ORM\Column(name="user_pass", type="string", length=255)
     * 
     *  
     */
    private $password;
    
    /**
     * @var string $email
     *
     * @ORM\Column(name="user_email", type="string", length=255)
     * 
     *  
     */
    private $email;
    
    /**
     * @ORM\OneToMany(targetEntity="PeopleMetaData", mappedBy="userId")
     */
    protected $metaDatas;
    
    /**
     * Needed for authentication
     */
    private $isActive;
    
    public function __construct() {
      $this->isActive = true;
      $this->metaDatas = new ArrayCollection();
    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Get displayname
     *
     * @return string 
     */
    public function getDisplayname()
    {
      if($this->displayname){
        return $this->displayname;
      }else{
        return $this->username;
      }
     
    }
    
    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
      return $this->password;
    }
    
    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
      return $this->email;
    }
    
  /**
   * Get metaDatas
   *
   * @return \PixeesBundle\Entity\PeopleMetaData 
   */
    public function getMetaDatas(){
      return $this->metaDatas;
    }
    
    /**
     * Get RolesArray
     *
     * @return array 
     */
    public function getRolesArray()
    {
      $criteria = Criteria::create()
        ->where(Criteria::expr()->eq('key', 'wp_capabilities')); // on recherche la metadata wp_capabilities                
      $metadata_capabilities = $this->metaDatas->matching($criteria);
      $capabilities = unserialize($metadata_capabilities[0]->getValue()); // on considère qu'il n'y a qu'une metada wp_capabilities
     
      $roles_array=array();
      foreach ($capabilities as $role => $valid) {
        if($valid){
          $roles_array[]=$role;
        }       
      }
      return $roles_array;
    } 
    
    /**
     * has role
     *
     * @return bool 
     */
    public function hasRole($role)
    {
      $roles_array = $this->getRolesArray();
      if(in_array($role,$roles_array)){
        return true;
      }else{
        return false;
      }   
    }  
    
    /**
     * Get rolesString
     *
     * @return string 
     */
    public function getRolesString()
    {
      $roles_array = $this->getRolesArray();
      $roles="";
      foreach ($roles_array as $role) {
        if($roles!=""){
          $roles.=" ; ";
        }
        $roles.=$role;
      }
      return $roles;
    }    
    
    public function getRoles()
    {
      if($this->hasRole('administrator')){
        return array('ROLE_ADMIN','ROLE_USER');
      }else{
        return array('ROLE_USER');
      }
    }
    
   
    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
      return $this->username;
    }
    
    public function eraseCredentials()
    {
    }
    
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
}