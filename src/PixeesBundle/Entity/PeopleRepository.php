<?php

namespace PixeesBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PeopleRepository
 *
 * Add your own custom
 * repository methods below.
 */
class PeopleRepository extends EntityRepository
{
   public function findAll(){
     return $this->findBy(array(), array('username' => 'ASC'));
   }
   
   public function findWithGeoLocBy($peopleId){
     $query = $this->getEntityManager()
        ->createQuery(
        "SELECT p,g FROM PixeesBundle:People p
        JOIN p.geoLoc g
        WHERE p.id = :id"
     )->setParameter('id', $peopleId);
          
     try {
       $result = $query->getSingleResult();             
       $people = $result;       
             
       return $people;
     } catch (\Doctrine\ORM\NoResultException $exception) {
       return null;
     }
   }
   
   public function findAllWithGeoLoc(){
     $query = $this->getEntityManager()
        ->createQuery(
        "SELECT p,g FROM PixeesBundle:People p
        left JOIN p.geoLoc g"
     );
          
     try {
       $results = $query->getResult();
       
       return $results;
     } catch (\Doctrine\ORM\NoResultException $exception) {
       return null;
     }
   }
   
   public function findAllWithRoles(){
     
   }
}
