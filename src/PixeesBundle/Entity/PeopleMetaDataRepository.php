<?php

namespace PixeesBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PeopleMetaDataRepository
 *
 * Add your own custom
 * repository methods below.
 */
class PeopleMetaDataRepository extends EntityRepository
{
   public function findAll(){
     return $this->findBy(array(), array('key' => 'ASC'));
   }
}
