<?php

namespace ClassCodeBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
          
        return true;
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
      global $kernel;
      $wp_path = $kernel->getContainer()->getParameter('wp_install_dir');     
      $login = '';
      
      $cwd = getcwd();
      chdir( $wp_path );
      include "wp-load.php";
      $wp_user = wp_get_current_user();
      if($wp_user->ID!=0){
        $login= $wp_user->user_login;
      }
      chdir( $cwd );      
      
      return array(
            'token' => $login,
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $login = $credentials['token'];
        if (null === $login) {
            return;
        }

        // if a User object, checkCredentials() is called
        return $userProvider->loadUserByUsername($login);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        // check credentials - e.g. make sure the password is valid
        // no credential check is needed in this case

        // return true to cause authentication success
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {        
        return null;
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
       return null;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}