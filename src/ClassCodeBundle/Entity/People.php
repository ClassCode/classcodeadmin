<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * ClassCodeBundle\Entity\People
 *
 * @ORM\Table(name="people")
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\PeopleRepository")
 */
class People 
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer $userId
     *
     * @ORM\Column(name="user_id", type="integer")
     *
     */
    private $userId;

    /**
     * @var string $username
     *
     * @ORM\Column(name="user_login", type="string", length=255)
     * 
     */
    private $username;
    
    /**
     * @var string $displayname
     *
     * @ORM\Column(name="displayName", type="string", length=255,nullable=true)
     * 
     */
    private $displayname;
    
        
    /**
     * @var string $email
     *
     * @ORM\Column(name="user_email", type="string", length=255)
     * 
     */
    private $email;
     
    /**  
     * @var string $avatar
     *
     * @ORM\Column(name="avatar", type="text",nullable=true)
     *  
     */
    private $avatar;
    
    /**  
     * @var string $avatarImg
     *
     * @ORM\Column(name="avatarImg", type="text",nullable=true)
     *  
     */
    private $avatarImg;

    /**  
     * @var string $firstname
     *
     * @ORM\Column(name="firstname", type="string", length=255,nullable=true)
     *  
     */
    private $firstname;
    
    /**  
     * @var string $lastname
     *
     * @ORM\Column(name="lastname", type="string", length=255,nullable=true)
     *  
     */
    private $lastname;
        
    /**  
     * @var string $nickname
     *
     * @ORM\Column(name="nickname", type="string", length=255,nullable=true)
     *  
     */
    private $nickname;
    
    /**  
     * @var string $structure
     *
     * @ORM\Column(name="structure", type="string", length=255,nullable=true)
     *  
     */
    private $structure;
    
    /**  
     * @var string $context
     *
     * @ORM\Column(name="context", type="text",nullable=true)
     *  
     */
    private $context;
    
    /**  
     * @var string $profile
     *
     * @ORM\Column(name="profile", type="string", length=255,nullable=true)
     *  
     */
    private $profile;
    
    /**  
     * @var string $teaching
     *
     * @ORM\Column(name="teaching", type="text",nullable=true)
     *  
     */
    private $teaching;
    
    /**
     * @ORM\Column(name="facilitator", type="boolean", length=1,options={"default" = 0})
     */
    private $facilitator = 0;
    
    /**  
     * @var string $computerJob
     *
     * @ORM\Column(name="computerJob", type="text",nullable=true)
     *  
     */
    private $computerJob;
    
    /**  
     * @var string $learning
     *
     * @ORM\Column(name="learning", type="text",nullable=true)
     *  
     */
    private $learning;

    /**  
     * @var string $skills
     *
     * @ORM\Column(name="skills", type="text",nullable=true)
     *  
     */
    private $skills;
      
    /**  
     * @var string $street
     *
     * @ORM\Column(name="street", type="text",nullable=true)
     *  
     */
    private $street;  
    
    /**  
     * @var string $city
     *
     * @ORM\Column(name="city", type="string", length=255,nullable=true)
     *  
     */
    private $city;
    
    /**  
     * @var string $state
     *
     * @ORM\Column(name="state", type="string", length=255,nullable=true)
     *  
     */
    private $state;
    
    /**  
     * @var string $zipcode
     *
     * @ORM\Column(name="zipcode", type="string", length=255,nullable=true)
     *  
     */
    private $zipcode;
    
    /**  
     * @var string $country
     *
     * @ORM\Column(name="country", type="string", length=255,nullable=true)
     *  
     */
    private $country;
    
    /**  
     * @var string $formattedAddress
     *
     * @ORM\Column(name="formattedAddress", type="text",nullable=true)
     *  
     */
    private $formattedAddress;
    
    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", length=255)
     * 
     */
    private $latitude = 0;
    
    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", length=255)
     * 
     */
    private $longitude = 0;
        
    /**
     * @ORM\ManyToMany(targetEntity="Meeting", mappedBy="subscribers",cascade={"persist"})
     **/
    private $meetings;
    
    /**
     * @ORM\OneToMany(targetEntity="Meeting", mappedBy="ownerId")
     **/
    private $ownedMeetings;
  
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="text",  type="string", length=255, nullable=true)
     */
    private $updated_by;
  
    public function __construct() {
      $this->isActive = true;
      $this->meetings = new ArrayCollection();
      $this->ownedMeetings = new ArrayCollection();
    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Set userId
     *
     * @param integer $userId
     * @return People
     */
    public function setUserId($userId)
    {
      $this->userId = $userId;
      return $this;
    }
    
    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
      return $this->userId;
    }
    
    /**
     * Set username
     *
     * @param string $username
     * @return People
     */
    public function setUsername($username)
    {
      $this->username = $username;
      return $this;
    }
    
    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
      return $this->username;
    }
    
    /**
     * Set displayname
     *
     * @param string $username
     * @return People
     */
    public function setDisplayname($displayname)
    {
      $this->displayname = $displayname;
      return $this;
    }
    
    /**
     * Get displayname
     *
     * @return string 
     */
    public function getDisplayname()
    {
      return $this->displayname;      
    }
    
    /**
     * Get commondisplayname
     *
     * @return string 
     */
    public function getCommonDisplayname()
    {
      if($this->nickname){
        return $this->nickname;
      }else if($this->lastname || $this->firstname){         
        if($this->firstname){
          $displayname = $this->firstname;
        }else{
          $displayname = "";
        }
        if($this->lastname){
          $displayname .= " ".$this->lastname;
        }
        return $displayname;
      }else if($this->displayname){
        return $this->displayname;
      }else{
        return $this->username;
      }
    }
    
    /**
     * Set email
     *
     * @param string $email
     * @return People
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return People
     */
    public function setAvatar($avatar)
    {
      $this->avatar = $avatar;
      return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
      return $this->avatar;
    }
       
    /**
     * Set avatarImg
     *
     * @param string $avatarImg
     * @return People
     */
    public function setAvatarImg($avatarImg)
    {
      $this->avatarImg = $avatarImg;
      return $this;
    }

    /**
     * Get avatarImg
     *
     * @return string 
     */
    public function getAvatarImg()
    {
      return $this->avatarImg;
    }
    
    /**
     * Set firstname
     *
     * @param string $firstname
     * @return People
     */
    public function setFirstname($firstname)
    {
      $this->firstname = $firstname;
      return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
      return $this->firstname;
    }
    
    /**
     * Set lastname
     *
     * @param string $lastname
     * @return People
     */
    public function setLastname($lastname)
    {
      $this->lastname = $lastname;
      return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
      return $this->lastname;
    }
    
    /**
     * Set nickname
     *
     * @param string $nickname
     * @return People
     */
    public function setNickname($nickname)
    {
      $this->nickname = $nickname;
      return $this;
    }

    /**
     * Get nickname
     *
     * @return string 
     */
    public function getNickname()
    {
      if($this->nickname){
        return $this->nickname;
      }else{
        return $this->displayname;
      }
    }    
    
    /**
     * Set structure
     *
     * @param string $structure
     * @return People
     */
    public function setStructure($structure)
    {
      $this->structure = $structure;
      return $this;
    }

    /**
     * Get structure
     *
     * @return string 
     */
    public function getStructure()
    {
      return $this->structure;
    }
        
    /**
     * Set context
     *
     * @param string $context
     * @return People
     */
    public function setContext($context)
    {
      $this->context = $context;
      return $this;
    }

    /**
     * Get context
     *
     * @return string 
     */
    public function getContext()
    {
      return $this->context;
    }
    
    /**
     * Set profile
     *
     * @param string $profile
     * @return People
     */
    public function setProfile($profile)
    {
      $this->profile = $profile;
      return $this;
    }

    /**
     * Get profile
     *
     * @return string 
     */
    public function getProfile()
    {
      return $this->profile;
    }
    
    /**
     * Set teaching
     *
     * @param string $teaching
     * @return People
     */
    public function setTeaching($teaching)
    {
      $this->teaching = $teaching;
      return $this;
    }

    /**
     * Get teaching
     *
     * @return string 
     */
    public function getTeaching()
    {
      return $this->teaching;
    }
    
    /**
     * Set facilitator
     *
     * @param boolean $facilitator
     * @return People
     */
    public function setFacilitator($facilitator)
    {
      $this->facilitator = $facilitator;
      return $this;
    }

    /**
     * Get facilitator
     *
     * @return boolean 
     */
    public function getFacilitator()
    {
      return $this->facilitator;
    }   
    
    /**
     * Set computerJob
     *
     * @param string $computerJob
     * @return People
     */
    public function setComputerJob($computerJob)
    {
      $this->computerJob = $computerJob;
      return $this;
    }

    /**
     * Get computerJob
     *
     * @return string 
     */
    public function getComputerJob()
    {
      return $this->computerJob;
    }
    
    /**
     * Set learning
     *
     * @param string $learning
     * @return People
     */
    public function setLearning($learning)
    {
      $this->learning = $learning;
      return $this;
    }

    /**
     * Get learning
     *
     * @return string 
     */
    public function getLearning()
    {
      return $this->learning;
    }
    
    /**
     * Set skills
     *
     * @param string $skills
     * @return People
     */
    public function setSkills($skills)
    {
      $this->skills = $skills;
      return $this;
    }

    /**
     * Get skills
     *
     * @return string 
     */
    public function getSkills()
    {
      return $this->skills;
    }
    
    /**
     * Set street
     *
     * @param string $street
     * @return People
     */
    public function setStreet($street)
    {
      $this->street = $street;
      return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
      return $this->street;
    }
    
    /**
     * Set city
     *
     * @param string $city
     * @return People
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
      return $this->city;
    }
    
    /**
     * Set state
     *
     * @param string $state
     * @return People
     */
    public function setState($state)
    {
      $this->state = $state;
      return $this;
    }

    /**
     * Get state
     *
     * @return state 
     */
    public function getState()
    {
      return $this->state;
    }
    
    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return People
     */
    public function setZipcode($zipcode)
    {
      $this->zipcode = $zipcode;
      return $this;
    }

    /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
      return $this->zipcode;
    }
    
    /**
     * Set country
     *
     * @param string $country
     * @return People
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
      return $this->country;
    }
    
    /**
     * Set formattedAddress
     *
     * @param string $formattedAddress
     * @return People
     */
    public function setFormattedAddress($formattedAddress)
    {
      $this->formattedAddress = $formattedAddress;
      return $this;
    }

    /**
     * Get formattedAddress
     *
     * @return string 
     */
    public function getFormattedAddress()
    {
      return $this->formattedAddress;
    }
    
    /**
     * Set latitude
     *
     * @param float $latitude
     * @return People
     */
    public function setLatitude($latitude)
    {
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
      return $this->latitude;
    }    
    
    /**
     * Set longitude
     *
     * @param float $longitude
     * @return People
     */
    public function setLongitude($longitude)
    {
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
      return $this->longitude;
    } 
    
    /**
     * Add Meeting
     * 
     * @param \ClassCodeBundle\Entity\Meeting
     * @return People
     */
   
    public function addMeeting(\ClassCodeBundle\Entity\Meeting $meeting)
    {
      if(!($this->meetings->contains($meeting))){    
        $this->meetings[] = $meeting;
      }
      return $this;
    }
    
    /**
     * Remove Meeting
     * 
     * @param \ClassCodeBundle\Entity\Meeting
     * @return People
     */
     
    public function removeMeeting(\ClassCodeBundle\Entity\Meeting $meeting)
    {
      if($this->meetings->contains($meeting)){
        $this->meetings->removeElement($meeting);
      }
      return $this;
    }
    /**
     * Get Meetings
     * 
     * @return Doctrine\Common\Collections\Collection
     */
    public function getMeetings()
    {
      return $this->meetings;
    }
    
    /**
     * Add $ownedMeetings
     *
     * @param \ClassCodeBundle\Entity\Meeting $ownedMeetings
     * @return People
     */
    public function addOwnedMeeting(\ClassCodeBundle\Entity\Meeting $ownedMeetings)
    {
      if(!($this->ownedMeetings->contains($ownedMeetings))){
        $this->ownedMeetings[] = $ownedMeetings;
      }
      return $this;
    }

    /**
     * Remove $ownedMeetings
     *
     * @param \ClassCodeBundle\Entity\Meetingm $ownedMeetings
     * @return People
     */
    public function removeOwnedMeeting(\ClassCodeBundle\Entity\Meeting $ownedMeetings)
    {
      if($this->ownedMeetings->contains($ownedMeetings)){
        $this->ownedMeetings->removeElement($ownedMeetings);
      }
    }

    /**
     * Get $ownedMeetings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOwnedMeetings()
    {
      return $this->ownedMeetings;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return People
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return People
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
    
}