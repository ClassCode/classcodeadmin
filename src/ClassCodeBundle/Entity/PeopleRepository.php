<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PeopleRepository
 *
 * Add your own custom
 * repository methods below.
 */
class PeopleRepository extends EntityRepository
{
   public function findAll(){
     return $this->findBy(array(), array('username' => 'ASC'));
   }
   
   public function findAllWithGeoLoc(){
     $query = $this->getEntityManager()
        ->createQuery(
        "SELECT p FROM ClassCodeBundle:People p
         WHERE p.latitude != 0 and p.longitude != 0"
     );
          
     try {
       $results = $query->getResult();
       
       return $results;
     } catch (\Doctrine\ORM\NoResultException $exception) {
       return null;
     }
   }

}
