<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * ClassCodeBundle\Entity\Structure
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\StructureRepository")
 */
class Structure
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     *  
     */
    private $name;
    
    /**
     * @var string $logo
     *
     * @ORM\Column(name="logo", type="string", length=255)
     * 
     *  
     */
    private $logo;
    
    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     * 
     *  
     */
    private $url;
    
    /**
     * @var string $who
     *
     * @ORM\Column(name="who", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $who;
    
    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * 
     *  
     */
    private $email;
    
    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255)
     * 
     *  
     */
    private $address;
    
    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", length=255)
     * 
     *  
     */
    private $latitude;
    
    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", length=255)
     * 
     *  
     */
    private $longitude;
    
    /**
     * @var string $mainType
     *
     * @ORM\Column(name="mainType", type="string", length=255)
     */
    private $mainType = 'Partenaire ClassCode';
    
    /**
     * @var string $secondaryType
     *
     * @ORM\Column(name="secondaryType", type="string", length=255, nullable=true)
     */
    private $secondaryType ;
    
    /**
     * @var string $dataSource
     *
     * @ORM\Column(name="dataSource", type="string", length=255)
     */
    private $dataSource = "web";
    
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="string", length=255, nullable=true)
     */
    private $updated_by;
    

    public function __construct() {

    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Structure
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
      return $this->name;
    }
    
    /**
     * Set logo
     *
     * @param string $logo
     * @return Structure
     */
    public function setLogo($logo)
    {
      $this->logo = $logo;
      return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
      return $this->logo;
    }
    
    
    /**
     * Set url
     *
     * @param string $url
     * @return Structure
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
      return $this->url;
    }    
    
    /**
     * Set who
     *
     * @param string $who
     * @return Structure
     */
    public function setWho($who)
    {
      $this->who = $who;
      return $this;
    }

    /**
     * Get who
     *
     * @return string 
     */
    public function getWho()
    {
      return $this->who;
    }    
    
    /**
     * Set email
     *
     * @param string $email
     * @return Structure
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
      return $this->email;
    }    
    
    /**
     * Set address
     *
     * @param string $address
     * @return Structure
     */
    public function setAddress($address)
    {
      $this->address = $address;
      return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
      return $this->address;
    }    
    
    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Structure
     */
    public function setLatitude($latitude)
    {
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
      return $this->latitude;
    }    
    
    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Structure
     */
    public function setLongitude($longitude)
    {
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
      return $this->longitude;
    }    
    
    /**
     * Set mainType
     *
     * @param string $mainType
     * @return Structure
     */
    public function setMainType($mainType)
    {
        $this->mainType = $mainType;
    
        return $this;
    }

    /**
     * Get mainType
     *
     * @return string 
     */
    public function getMainType()
    {
        return $this->mainType;
    }

    /**
     * Set secondaryType
     *
     * @param string $secondaryType
     * @return Structure
     */
    public function setSecondaryType($secondaryType)
    {
        $this->secondaryType = $secondaryType;
    
        return $this;
    }

    /**
     * Get secondaryType
     *
     * @return string 
     */
    public function getSecondaryType()
    {
        return $this->secondaryType;
    }
        
    /**
     * Set dataSource
     *
     * @param string $dataSource
     * @return Structure
     */
    public function setDataSource($dataSource)
    {
        $this->dataSource = $dataSource;
    
        return $this;
    }

    /**
     * Get dataSource
     *
     * @return string 
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return Structure
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return Structure
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}