<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MeetingRepository
 *
 * Add your own custom
 * repository methods below.
 */
class MeetingRepository extends EntityRepository
{
   public function findAll(){
     return $this->findBy(array(), array('date' => 'ASC'));
   }
   
   public function findAllWithGeoLoc(){
     $query = $this->getEntityManager()
        ->createQuery(
        "SELECT m FROM ClassCodeBundle:Meeting m
         WHERE m.latitude != 0 and m.longitude != 0"
     );
          
     try {
       $results = $query->getResult();
       
       return $results;
     } catch (\Doctrine\ORM\NoResultException $exception) {
       return null;
     }
   }

}
