<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ClassCodeBundle\Entity\Meeting
 *
 * @ORM\Table(name="meeting")
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\MeetingRepository")
 */
class Meeting
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer $ownerId 
     *     * 
     * @ORM\ManyToOne(targetEntity="People", inversedBy="ownedMeetings")
     * @ORM\JoinColumn(name="ownerId", referencedColumnName="id", nullable=false)
     * @Assert\Type(type="ClassCodeBundle\Entity\People")
     *
     */
    protected $ownerId;
    
    /**
     * @var string $date
     *
     * @ORM\Column(name="date", type="string", length=255 , nullable=true)
     */
    private $date;
    
    /**
     * @var string $time
     *
     * @ORM\Column(name="time", type="string", length=255 , nullable=true)
     */
    private $time;
    
    /**  
     * @var string $structure
     *
     * @ORM\Column(name="structure", type="string", length=255,nullable=true)
     *  
     */
    private $structure;
    
    /**  
     * @var string $street
     *
     * @ORM\Column(name="street", type="text",nullable=true)
     *  
     */
    private $street;  
    
    /**
     * @var string $city
     *
     * @ORM\Column(name="city", type="text", length=255,nullable=true)
     * 
     *  
     */
    private $city;
        
    /**
     * @var string $zipcode
     *
     * @ORM\Column(name="zipcode", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $zipcode;
    
    /**  
     * @var string $country
     *
     * @ORM\Column(name="country", type="string", length=255,nullable=true)
     *  
     */
    private $country;
    
    /**  
     * @var string $state
     *
     * @ORM\Column(name="state", type="string", length=255,nullable=true)
     *  
     */
    private $state;
    
    /**  
     * @var string $formattedAddress
     *
     * @ORM\Column(name="formattedAddress", type="text",nullable=true)
     *  
     */
    private $formattedAddress;
    
    /**
     * @var string $capacity
     *
     * @ORM\Column(name="capacity", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $capacity;
    
    /**
     * @var string $hangoutLink
     *
     * @ORM\Column(name="hangoutLink", type="text",nullable=true)
     * 
     *  
     */
    private $hangoutLink;
    
    /**
     * @var string $more
     *
     * @ORM\Column(name="more", type="text",nullable=true)
     * 
     *  
     */
    private $more;
    
    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="text",nullable=true)
     * 
     *  
     */
    private $subject;
    
    /**
     * @var string $precisions
     *
     * @ORM\Column(name="precisions", type="text",nullable=true)
     * 
     *  
     */
    private $precisions;
  
    /**
     * @ORM\ManyToMany(targetEntity="People", inversedBy="meetings",cascade={"persist"})
     * @ORM\JoinTable(name="meetings_subscribers")
     **/
    private $subscribers;
    
    
    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", length=255)
     * 
     *  
     */
    private $latitude = 0;
    
    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", length=255)
     * 
     *  
     */
    private $longitude = 0;
    
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="text",  type="string", length=255, nullable=true)
     */
    private $updated_by;

    public function __construct() {
      $this->subscribers = new ArrayCollection();
    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Get people
     *
     * @return \ClassCodeBundle\Entity\People 
     */
    public function getOwner()
    {
      return $this->ownerId;
    }
    
    /**
     * Set people
     *
     * @param \ClassCodeBundle\Entity\People $people
     * @return GeoLoc
     */
    public function setOwner(\ClassCodeBundle\Entity\People $people)
    {
      $this->ownerId = $people;
      return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
      return $this->date;
    }    
    
    /**
     * Set date
     *
     * @param string $date
     * @return Meeting
     */
    public function setDate($date)
    {
      $this->date = $date;
      return $this;
    }
    
    /**
     * Get time
     *
     * @return string 
     */
    public function getTime()
    {
      return $this->time;
    }    
    
    /**
     * Set time
     *
     * @param string $time
     * @return Meeting
     */
    public function setTime($time)
    {
      $this->time = $time;
      return $this;
    }
    
    /**
     * Get structure
     *
     * @return string 
     */
    public function getStructure()
    {
      return $this->structure;
    }
    
    /**
     * Set structure
     *
     * @param string $structure
     * @return Meeting
     */
    public function setStructure($structure)
    {
      $this->structure = $structure;
      return $this;
    }
    
    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
      return $this->street;
    }
    
    /**
     * Set street
     *
     * @param string $street
     * @return Meeting
     */
    public function setStreet($street)
    {
      $this->street = $street;
      return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
      return $this->city;
    }    
    
    /**
     * Set town
     *
     * @param string $city
     * @return Meeting
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }
    
    /**
     * Get state
     *
     * @return state 
     */
    public function getState()
    {
      return $this->state;
    }
    
    /**
     * Set state
     *
     * @param string $state
     * @return Meeting
     */
    public function setState($state)
    {
      $this->state = $state;
      return $this;
    }

   /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
      return $this->zipcode;
    }
    
    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Meeting
     */
    public function setZipcode($zipcode)
    {
      $this->zipcode = $zipcode;
      return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
      return $this->country;
    }
    
    /**
     * Set country
     *
     * @param string $country
     * @return Meeting
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

   /**
     * Get formattedAddress
     *
     * @return string 
     */
    public function getFormattedAddress()
    {
      return $this->formattedAddress;
    }
        
    /**
     * Set formattedAddress
     *
     * @param string $formattedAddress
     * @return Meeting
     */
    public function setFormattedAddress($formattedAddress)
    {
      $this->formattedAddress = $formattedAddress;
      return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
      return $this->latitude;
    }    
    
    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Meeting
     */
    public function setLatitude($latitude)
    {
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
      return $this->longitude;
    } 
      
    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Meeting
     */
    public function setLongitude($longitude)
    {
      $this->longitude = $longitude;
      return $this;
    }


    /**
     * Get capacity
     *
     * @return string 
     */
    public function getCapacity()
    {
      return $this->capacity;
    }    
    
    /**
     * Set capacity
     *
     * @param string $capacity
     * @return Meeting
     */
    public function setCapacity($capacity)
    {
      $this->capacity = $capacity;
      return $this;
    }
    
    /**
     * Get hangoutLink
     *
     * @return string 
     */
    public function getHangoutLink()
    {
      return $this->hangoutLink;
    }    
    
    /**
     * Set hangoutLink
     *
     * @param string $hangoutLink
     * @return Meeting
     */
    public function setHangoutLink($hangoutLink)
    {
      $this->hangoutLink = $hangoutLink;
      return $this;
    }
    
    /**
     * Get more
     *
     * @return string 
     */
    public function getMore()
    {
      return $this->more;
    }    
    
    /**
     * Set more
     *
     * @param string $more
     * @return Meeting
     */
    public function setMore($more)
    {
      $this->more = $more;
      return $this;
    }
    
    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
      return $this->subject;
    }    
    
    /**
     * Set subject
     *
     * @param string $subject
     * @return Meeting
     */
    public function setSubject($subject)
    {
      $this->subject = $subject;
      return $this;
    }
    
    /**
     * Get precisions
     *
     * @return string 
     */
    public function getPrecisions()
    {
      return $this->precisions;
    }    
    
    /**
     * Set precisions
     *
     * @param string $precisions
     * @return Meeting
     */
    public function setPrecisions($precisions)
    {
      $this->precisions = $precisions;
      return $this;
    }
      
    /**
     *  Add Subscriber
     * 
     * @param \ClassCodeBundle\Entity\People
     * @return Meeting
     */
   
    public function addSubscriber(\ClassCodeBundle\Entity\People $subscriber)
    { 
      if(!($this->subscribers->contains($subscriber))){
        $subscriber->addMeeting($this); // synchronously updating inverse side
        $this->subscribers[] = $subscriber;
      }
      return $this;
    }
    
    /**
     *  Remove Subscriber
     * 
     *  @param \ClassCodeBundle\Entity\People
     *  @return Meeting
     */
     
    public function removeSubscriber(\ClassCodeBundle\Entity\People $subscriber)
    {
      if($this->subscribers->contains($subscriber)){
        $subscriber->removeMeeting($this); // synchronously updating inverse side
        $this->subscribers->removeElement($subscriber);
      }
      return $this;
    }
    /**
     *  Get Subscribers
     * 
     * @return Doctrine\Common\Collections\Collection
     */
    public function getSubscribers()
    {
      return $this->subscribers;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return Meeting
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return Meeting
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
    
}