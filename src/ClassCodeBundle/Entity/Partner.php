<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ClassCodeBundle\Entity\Group;


/**
 * ClassCodeBundle\Entity\Partner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\PartnerRepository")
 */
class Partner
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     *  
     */
    private $name;
    
    /**
     * @var string $logo
     *
     * @ORM\Column(name="logo", type="string", length=255)
     * 
     *  
     */
    private $logo;
    
    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     * 
     *  
     */
    private $url;
    
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="text",  type="string", length=255, nullable=true)
     */
    private $updated_by;
    
     /**
     * @var integer $groupId
     *
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="partners")
     * @ORM\JoinColumn(name="groupId", referencedColumnName="id", nullable=false)
     * @Assert\Type(type="ClassCodeBundle\Entity\Group")
     */
    protected $groupId;

    public function __construct() {

    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Partner
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
      return $this->name;
    }
    
    /**
     * Set logo
     *
     * @param string $logo
     * @return Partner
     */
    public function setLogo($logo)
    {
      $this->logo = $logo;
      return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
      return $this->logo;
    }
    
    /**
     * Get logoUrl
     *
     * @return string 
     */
    public function getLogoUrl()
    {
      if($this->getGroup() && $this->getLogo()){
        return "/wp-content/plugins/class_code_v2/partenaires/LogosPartenaires/".$this->getGroup()->getName()."/".$this->getLogo();  
      }else{
        return "";
      }
      
    }
    
    /**
     * Set url
     *
     * @param string $url
     * @return Partner
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
      return $this->url;
    }    
    
    /**
     * Set Group
     *
     * @param \ClassCodeBundle\Entity\Group $groupId
     * @return Partner
     */
    public function setGroup(\ClassCodeBundle\Entity\Group $groupId)
    {
      $this->groupId = $groupId;
      return $this;
    }

    /**
     * Get Group
     *
     * @return ClassCodeBundle\Entity\Group 
     */
    public function getGroup()
    {
      return $this->groupId;
    }
  

    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return Partner
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return Partner
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}