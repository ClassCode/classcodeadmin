<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * ClassCodeBundle\Entity\Coordination
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\CoordinationRepository")
 */
class Coordination
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     *  
     */
    private $name;
    
    /**
     * @var string $classCodePage
     *
     * @ORM\Column(name="classCodePage", type="string", length=255)
     * 
     *  
     */
    private $classCodePage;
    
    /**
     * @var string $logo
     *
     * @ORM\Column(name="logo", type="string", length=255)
     * 
     *  
     */
    private $logo;
    
    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     * 
     *  
     */
    private $url;
    
    /**
     * @var string $who
     *
     * @ORM\Column(name="who", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $who;
    
    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * 
     *  
     */
    private $email;
    
    /**
     * @var string $emails
     *
     * @ORM\Column(name="emails", type="string", length=255, nullable=true)
     * 
     *  
     */
    private $emails;
    
    /**
     * @var string $slug
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     * 
     *  
     */
    private $slug;
    
    /**
     * @var string region
     *
     * @ORM\Column(name="region", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $region;
    
    /**
     * @var string structure
     *
     * @ORM\Column(name="structure", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $structure;
    
    /**
     * @ORM\Column(name="alert", type="boolean", length=1,options={"default" = 1})
     */
    private $alert = 1;
    
    /**
     * @var string $address
     *
     * @ORM\Column(name="address", type="string", length=255,nullable=true)
     * 
     *  
     */
    private $address;
    
    /**
     * @var float $latitude
     *
     * @ORM\Column(name="latitude", type="float", length=255,nullable=true)
     * 
     *  
     */
    private $latitude;
    
    /**
     * @var float $longitude
     *
     * @ORM\Column(name="longitude", type="float", length=255,nullable=true)
     * 
     *  
     */
    private $longitude;
    
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="text",  type="string", length=255, nullable=true)
     */
    private $updated_by;
    

    public function __construct() {

    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Coordination
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
      return $this->name;
    }
     
    /**
     * Set classCodePage
     *
     * @param string $classCodePage
     * @return Coordination
     */
    public function setClassCodePage($classCodePage)
    {
      $this->classCodePage = $classCodePage;
      return $this;
    }

    /**
     * Get classCodePage
     *
     * @return string 
     */
    public function getClassCodePage()
    {
      return $this->classCodePage;
    }
    
    /**
     * Set logo
     *
     * @param string $logo
     * @return Coordination
     */
    public function setLogo($logo)
    {
      $this->logo = $logo;
      return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
      return $this->logo;
    }
    
    
    /**
     * Set url
     *
     * @param string $url
     * @return Coordination
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
      return $this->url;
    }    
    
    /**
     * Set who
     *
     * @param string $who
     * @return Coordination
     */
    public function setWho($who)
    {
      $this->who = $who;
      return $this;
    }

    /**
     * Get who
     *
     * @return string 
     */
    public function getWho()
    {
      return $this->who;
    }    
    
    /**
     * Set email
     *
     * @param string $email
     * @return Coordination
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
      return $this->email;
    }  
    
    /**
     * Set emails
     *
     * @param string $emails
     * @return Coordination
     */
    public function setEmails($emails)
    {
      $this->emails = $emails;
      return $this;
    }

    /**
     * Get emails
     *
     * @return string 
     */
    public function getEmails()
    {
      return $this->emails;
    }    

    /**
     * Set slug
     *
     * @param string $slug
     * @return Coordination
     */
    public function setSlug($slug)
    {
      $this->slug = $slug;
      return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
      return $this->slug;
    }    
    
    /**
     * Set region
     *
     * @param string $region
     * @return Coordination
     */
    public function setRegion($region)
    {
      $this->region = $region;
      return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
      return $this->region;
    }    
    
    /**
     * Set structure
     *
     * @param string $structure
     * @return Coordination
     */
    public function setStructure($structure)
    {
      $this->structure = $structure;
      return $this;
    }

    /**
     * Get structure
     *
     * @return string 
     */
    public function getStructure()
    {
      return $this->structure;
    }    
    
    /**
     * Set alert
     *
     * @param boolean $alert
     * @return Coordination
     */
    public function setAlert($alert)
    {
      $this->alert = $alert;
      return $this;
    }

    /**
     * Get alert
     *
     * @return boolean 
     */
    public function getAlert()
    {
      return $this->alert;
    }   
    
    /**
     * Set address
     *
     * @param string $address
     * @return Coordination
     */
    public function setAddress($address)
    {
      $this->address = $address;
      return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
      return $this->address;
    }    
    
    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Coordination
     */
    public function setLatitude($latitude)
    {
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
      return $this->latitude;
    }    
    
    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Coordination
     */
    public function setLongitude($longitude)
    {
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
      return $this->longitude;
    }    
    
    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return Coordination
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return Coordination
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}