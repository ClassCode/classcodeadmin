<?php

namespace ClassCodeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert ;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ClassCodeBundle\Entity\Partner;


/**
 * ClassCodeBundle\Entity\Group
 *
 * @ORM\Table(name="partnerGroup")
 * @ORM\Entity(repositoryClass="ClassCodeBundle\Entity\GroupRepository")
 */
class Group
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * 
     *  
     */
    private $name;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Partner", mappedBy="groupId")
     */
    protected $partners;
    
    /**
     * @var \DateTime $updated_at
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    /**
     * @var string $updated_by
     *
     * @ORM\Column(name="updated_by", type="text",  type="string", length=255, nullable=true)
     */
    private $updated_by;
    
    public function __construct() {

    }
     
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
      return $this->id;
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
      return $this->name;
    }
     
    /**
     * Add $partner
     *
     * @param \ClassCodeBundle\Entity\Partner $partner
     * @return Group
     */
    public function addPartner(\ClassCodeBundle\Entity\Partner $partner)
    {
      if(!($this->partners->contains($partner))){
        $this->partners[] = $partner;
      }
      return $this;
    }

    /**
     * Remove $partner
     *
     * @param \ClassCodeBundle\Entity\Partner $partner
     * @return Group
     */
    public function removePartner(\ClassCodeBundle\Entity\Partner $partner)
    {
      if($this->partners->contains($partner)){
        $this->partners->removeElement($partner);
      }
    }

    /**
     * Get $partners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPartners()
    {
      return $this->partners;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $date
     * @return Group
     */
    public function setUpdatedAt($date)
    {
        $this->updated_at = $date;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * Set updated_by
     *
     * @param string $login
     * @return Group
     */
    public function setUpdatedBy($login)
    {
        $this->updated_by = $login;
    
        return $this;
    }

    /**
     * Get updated_by
     *
     * @return string 
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }
}