<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;


class TestMailCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:TestMail')
    ->setDescription('Test email sending')
    ->addArgument('email', InputArgument::OPTIONAL, 'email to alert to, default is benjamin.ninassi@inria.fr')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');
    
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  $old_time = time();

    $email = $input->getArgument('email');
    if(!$email){
      $email="benjamin.ninassi@inria.fr";
    }
    $logo_error_tab = array("Ceci est une erreur de test");
    $this->send_email($logo_error_tab,$email);
    
    $new_time = time();  
    print "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    print "Done, exiting ! \n";
  }

  private function send_email($logo_error_tab,$email){
      
    $container = $this->getContainer();
    $to = $email;
    $subject = "[ClassCodeAdmin]Erreur de Logo de structure";
    //envoi de mail
    $body = "<br>";
    $body.= "Bonjour,<br>";
    $body.= "Ce mail est envoye automatiquement pour le site classcodeadmin. ";
    $body.= "<br>";
    $body.= "Voici la liste des erreurs :<br>";
    foreach($logo_error_tab as $error){
      $body.= $error."<br>";
    }    
    
    $message = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom("noreply@classcodeadmin.inria.fr")
      ->setTo($to)
      ->setBody($body)
      ->setContentType('text/html')
    ;
    
    $mailer = $container->get('mailer');
    $mailer->send($message);
  }
}

	    			