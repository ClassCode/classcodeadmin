<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\Structure;

class ImportStructuresFromJsonCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importStructuresFromJson')
    ->setDescription('Import datas from Json files')
    ->addArgument('file', InputArgument::REQUIRED, 'json file containing datas')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');
    $userName = "Import"; // pour le updatedBy
 
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importStructuresFromJson.log';
    $log_file_path_error = $log_dir.'/importStructuresFromJson.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import Structures ... \n";
    $old_time = time();   

    $structureFile=$input->getArgument('file');
    
    if(!is_file($structureFile)){
      $log_msg_error .= "File ".$structureFile." not found, aborting import !\n";
      $log_msg .= "File ".$structureFile." not found, aborting import !\n";
      file_put_contents($log_file_path, $log_msg);
      file_put_contents($log_file_path_error, $log_msg_error);
      return false;
    }else{
      $dataSource = basename($structureFile);
      $json_source = file_get_contents($structureFile);
      $json_array = json_decode($json_source,true); 
    }
    if($json_array['structures']){         
      foreach ($json_array['structures'] as $data_structure) {
        if($data_structure['type'] && $data_structure['name']){
          $type = ltrim(rtrim($data_structure['type']));      
          $name = ltrim(rtrim($data_structure['name']));
          if($type && $name){
            if($data_structure['logo']){
              $logo = ltrim(rtrim($data_structure['logo']));  
            }else{
              $logo ="";
            }
            if($data_structure['structureType']){
              $secondaryType = ltrim(rtrim($data_structure['structureType']));  
            }else{
              $secondaryType ="";
            }
            if($data_structure['url']){
              $url = ltrim(rtrim($data_structure['url']));  
            }else{
              $url="";
            }
            if($data_structure['who']){
              $who = ltrim(rtrim($data_structure['who']));  
            }else{
              $who = "";
            }
            if($data_structure['address']){
              $address = ltrim(rtrim($data_structure['address']));  
            }else{
              $address="";
            }
            if($data_structure['location']){
              $location = $data_structure['location'];
              if($location && is_array($location) && $location['lat'] && $location['lng']){
                $lat = ltrim(rtrim($location['lat']));
                $long = ltrim(rtrim($location['lng']));
              }else{
                $lat = 0;
                $long = 0;
              }  
            }else{
              $lat = 0;
              $long = 0;
            }
            if($data_structure['email']){
              $email = ltrim(rtrim($data_structure['email']));  
            }else{
              $email="";
            }            
      
            $structure =  $em->getRepository('ClassCodeBundle:Structure')->findOneBy(array('name' => $name,'mainType' => $type) );
            
            if(!$structure){
              $structure = new Structure();   
              $structure->setName($name);  
              $structure->setMainType($type);
            }
            if(($structure->getLogo() != $logo)||
               ($structure->getSecondaryType() != $secondaryType)||
               ($structure->getUrl() != $url)||
               ($structure->getWho()!=$who)||
               ($structure->getAddress() != $address)||
               ($structure->getLatitude() != $lat)||
               ($structure->getLongitude() != $long)||
               ($structure->getDataSource() != $dataSource)||
               ($structure->getEmail() != $email)){
                 
              $structure->setLogo($logo); 
              $structure->setSecondaryType($secondaryType);
              $structure->setUrl($url); 
              $structure->setWho($who); 
              $structure->setEmail($email); 
              $structure->setAddress($address); 
              $structure->setLatitude($lat); 
              $structure->setLongitude($long); 
              $structure->setDataSource($dataSource);
              $structure->setUpdatedAt(new \DateTime());
              $structure->setUpdatedBy($userName);   
              
              $em->persist($structure);
              $em->flush();
            }    
          }  
        }else{
          $log_msg_error .= "No type or no name defined, structure passed !\n";  
        }
      }
    }else{
      $log_msg_error .= "No structures found, aborting import !\n";    
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			