<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\People;

class ImportPeopleCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importPeople')
    ->setDescription('Import datas about people from wordpress and legacy classcode plugin')    
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager();
    $emp = $this->getContainer()->get('doctrine')->getManager('pixees');
    $userName = "Import"; // pour le updatedBy
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importPeople.log';
    $log_file_path_error = $log_dir.'/importPeople.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import People ... \n";
    $old_time = time();      

    $rawSql = "SELECT p.id,p.user_login,p.display_name,p.user_email,g.street,g.city, g.state, g.zipcode,g.country_long, g.formatted_address, g.lat, g.long FROM wp_users p left join wppl_friends_locator g on g.member_id = p.id ;";
    $stmt = $emp->getConnection()->prepare($rawSql);
    $stmt->execute();
    $people_in_wp = $stmt->fetchAll();

    foreach ($people_in_wp as $p) {
      //cleaning
      if((!$p['lat']) || ($p['lat']=='NULL') || ($p['lat'] =='')){
        $p['lat'] = 0;
      }
      if((!$p['long']) || ($p['long']=='NULL') || ($p['long'] =='')){
        $p['long'] = 0;
      }
      $people = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' => $p['id']));
      if(!$people){        
        $people = new People(); 
      }      
      $people->setUserId($p['id']);
      $people->setUsername($p['user_login']);
      $people->setDisplayname($p['display_name']);
      $people->setEmail($p['user_email']);
      
      //recuperation des donnnées de profil budypress
      $rawSql = "SELECT f.name,d.value FROM wp_bp_xprofile_data d inner join wp_bp_xprofile_fields f WHERE f.id = d.field_id and user_id = ".$p['id'].";";
      $stmt = $emp->getConnection()->prepare($rawSql);
      $stmt->execute();
      $profileData = $stmt->fetchAll();
      foreach ($profileData as $data) {
        switch($data['name']){
          case "Avatar":
            $people->setAvatar($data['value']);
            break;
          case "AvatarImg":
            $people->setAvatarImg($data['value']);
            break;          
          case "Prénom":
            $people->setFirstname($data['value']);
            break;
          case "Nom":
            $people->setLastname($data['value']);
            break; 
          case "Pseudo":
            $people->setNickname($data['value']);
            break;
          case "Structure":
            $people->setStructure($data['value']);
            break;
          case "Cadre":
            $people->setContext($data['value']);
            break;
          case "Profil":
            $people->setProfile($data['value']);
            break;
          case "Matière enseignée":
            $people->setTeaching($data['value']);
            break;
          case "Facilitateur":
            $facTab = unserialize($data['value']);
            $facilitator = 0;
            if((sizeof($facTab)>0) && ($facTab[0] == "Je veux bien être facilitateur")){
              $facilitator = 1;
            }
            $people->setFacilitator($facilitator);
            break;
          case "Chercheur Etudiant Industriel Precision":
            $people->setComputerJob($data['value']);
            break;
          case "Matière suivie":
            $people->setLearning($data['value']);
            break;
          case "Mes compétences":
            $people->setSkills($data['value']);
            break;         
        }
      }
      
      //recuperation des données de geoloc
      $people->setStreet($p['street']);
      $people->setCity($p['city']);
      $people->setState($p['state']);
      $people->setZipcode($p['zipcode']);
      $people->setCountry($p['country_long']);
      $people->setFormattedAddress($p['formatted_address']);
      $people->setLatitude($p['lat']);
      $people->setLongitude($p['long']);
      
      $people->setUpdatedAt(new \DateTime());
      $people->setUpdatedBy($userName);   
      $em->persist($people);
      $em->flush();  
     
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			