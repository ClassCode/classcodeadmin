<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\Group;
use ClassCodeBundle\Entity\Partner;

class ImportPartnersCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importPartners')
    ->setDescription('Import datas from legacy classcode files')
    ->addArgument('dir', InputArgument::OPTIONAL, 'pixees root directory, default is /appli/www/pixees/')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');
    $userName = "Import"; // pour le updatedBy
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importPartners.log';
    $log_file_path_error = $log_dir.'/importPartners.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import Partners ... \n";
    $old_time = time();   
    $root_dir = $input->getArgument('dir');
    
    if (!$root_dir){
      $root_dir="/appli/www/pixees/"; //default value (prod)
    }
    $partnerFile=$root_dir.'wp-content/plugins/class_code_v2/partenaires/data_partenaires.php';

    if(!is_file($partnerFile)){
      $log_msg .= "File ".$partnerFile." not found in ".$root_dir." , aborting import !\n";
      file_put_contents($log_file_path, $log_msg);
      file_put_contents($log_file_path_error, $log_msg_error);
      return false;
    }else{
      include $partnerFile; //$data_partenaires contain all informations now
    }
        
    foreach ($data_partenaires as $data_partenaire) {
      $PartnerName = ltrim(rtrim($data_partenaire['name']));   
      $PartnerLogo = ltrim(rtrim($data_partenaire['logo']));   
      $PartnerUrl = ltrim(rtrim($data_partenaire['url']));   
      $GroupName = ltrim(rtrim($data_partenaire['group']));
      
      $group =  $em->getRepository('ClassCodeBundle:Group')->findOneBy(array('name' => $GroupName));  
      $partner =  $em->getRepository('ClassCodeBundle:Partner')->findOneBy(array('name' => $PartnerName));
      
      if(!$group){
        $group = new Group();    
        $group->setName($GroupName); 
        $group->setUpdatedAt(new \DateTime());
        $group->setUpdatedBy($userName);   
        $em->persist($group);
        $em->flush();  
      }
      
      if(!$partner){
        $partner = new Partner();   
        $partner->setName($PartnerName);           
      }
      if(($partner->getLogo() != $PartnerLogo)||($partner->getUrl() != $PartnerUrl)||($partner->getGroup()!=$group)){
        $partner->setLogo($PartnerLogo); 
        $partner->setUrl($PartnerUrl); 
        $partner->setGroup($group); 
        $partner->setUpdatedAt(new \DateTime());
        $partner->setUpdatedBy($userName);   
        $em->persist($partner);
        $em->flush();
      }      
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			