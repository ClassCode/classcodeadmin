<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\Structure;

class VerifyStructuresLogoCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:VerifyStructuresLogo')
    ->setDescription('Verify existance of logo files and mail alert')
    ->addArgument('email', InputArgument::OPTIONAL, 'email to alert to, default is benjamin.ninassi@inria.fr')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');    
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  $old_time = time();
    $email = $input->getArgument('email');
    if(!$email){
      $email="benjamin.ninassi@inria.fr";
    }
    
    $structures =  $em->getRepository('ClassCodeBundle:Structure')->findby(array('mainType' => 'Partenaire Classcode')); 
    $structuresLogos = array();

    $logo_error_tab=array();
    print "Verification begin !\n";
    foreach ($structures as $structure) {
      $logo = $structure->getLogo();      
      if($logo){
        $code = $this->is_url_exist($logo);
        if($code != "200"){
          $temp_error_log ="Le logo ".$logo." n'a pas été trouvé ";
          if($code == "404"){
            $temp_error_log.= "(erreur 404 : file not found) ";
          }else if($code == "0"){
            $temp_error_log.= "(erreur 0 : le serveur est injoignable ?) ";
          }else{
            $temp_error_log.= "(erreur ".$code.") ";
          }
          $temp_error_log.= "pour la structure <b>".$structure->getName()."</b>";
          $logo_error_tab[] = $temp_error_log;
        }
      }else{
        $logo_error_tab[]="Pas de logo défini pour la structure <b>".$structure->getName()."</b>";  
      }      
    }
           
    $em->clear();    
    print "Verification finished !\n";
    print sizeof($logo_error_tab)." errors \n";
    if(sizeof($logo_error_tab)!=0){
      $this->send_email($logo_error_tab,$email);
    }
    $new_time = time();  
    print "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    print "Done, exiting ! \n";
  }

  private function send_email($logo_error_tab,$email){
    $wp_host = $this->getContainer()->getParameter('wp_home');  
    $container = $this->getContainer();
    $to = $email;
    $subject = "[ClassCodeAdmin]Erreur de Logo de structure";
    //envoi de mail
    $body = "<br>";
    $body.= "Bonjour,<br>";
    $body.= "Ce mail est envoye automatiquement par le site <a href='".$wp_host."/classcodeadmin'>".$wp_host."/classcodeadmin</a>. ";
    $body.= "<br>";
    $body.= "Vous pouvez corriger ces erreurs sur la page de gestion des structures : <a href='".$wp_host."/classcodeadmin/structures'>".$wp_host."/classcodeadmin/structures</a>";
    $body.= "<br>";
    $body.= "Voici la liste des erreurs :<br>";
    foreach($logo_error_tab as $error){
      $body.= $error."<br>";
    }    
    
    $message = \Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom("noreply@classcodeadmin.inria.fr")
      ->setTo($to)
      ->setBody($body)
      ->setContentType('text/html')
    ;
    
    $mailer = $container->get('mailer');
    $mailer->send($message);
  }

  private function is_url_exist($url){
    $ch = \curl_init($url);    
    \curl_setopt($ch, CURLOPT_NOBODY, true);
    \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    \curl_setopt($ch, CURLOPT_ENCODING, "");
    \curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    \curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    \curl_exec($ch);
    $code = \curl_getinfo($ch, CURLINFO_HTTP_CODE);

    \curl_close($ch);
   return $code;
  }
}

	    			