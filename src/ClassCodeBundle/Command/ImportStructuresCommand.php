<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\Structure;

class ImportStructuresCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importStructures')
    ->setDescription('Import datas from legacy classcode files')
    ->addArgument('dir', InputArgument::OPTIONAL, 'pixees root directory, default is /appli/www/pixees/')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');
    $userName = "Import"; // pour le updatedBy
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importStructures.log';
    $log_file_path_error = $log_dir.'/importStructures.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import Structures ... \n";
    $old_time = time();   
    $root_dir = $input->getArgument('dir');
    
    if (!$root_dir){
      $root_dir="/appli/www/pixees/"; //default value (prod)
    }
    $structureFile=$root_dir.'wp-content/plugins/class_code_v2/structures/ClassCode_config_structures.php';

    if(!is_file($structureFile)){
      $log_msg .= "File ".$structureFile." not found in ".$root_dir." , aborting import !\n";
      file_put_contents($log_file_path, $log_msg);
      file_put_contents($log_file_path_error, $log_msg_error);
      return false;
    }else{
      include $structureFile; //$data_structures contain all informations now
    }
        
    foreach ($data_structures as $data_structure) {
      $name = ltrim(rtrim($data_structure['name']));   
      $logo = ltrim(rtrim($data_structure['logo']));   
      $url = ltrim(rtrim($data_structure['url']));   
      $who = ltrim(rtrim($data_structure['who']));
      $address = ltrim(rtrim($data_structure['address']));
      $location = $data_structure['location'];
      if($location && is_array($location)){
        $lat = ltrim(rtrim($location['lat']));
        $long = ltrim(rtrim($location['lng']));
      }else{
        $lat = 0;
        $long = 0;
      }
      $email = ltrim(rtrim($data_structure['email']));

      $structure =  $em->getRepository('ClassCodeBundle:Structure')->findOneBy(array('name' => $name));
      
      if(!$structure){
        $structure = new Structure();   
        $structure->setName($name);            
      }
      if(($structure->getLogo() != $logo)||
         ($structure->getUrl() != $url)||
         ($structure->getWho()!=$who)||
         ($structure->getAddress() != $address)||
         ($structure->getLatitude() != $lat)||
         ($structure->getLongitude() != $long)||
         ($structure->getEmail() != $email)){
           
        $structure->setLogo($logo); 
        $structure->setUrl($url); 
        $structure->setWho($who); 
        $structure->setEmail($email); 
        $structure->setAddress($address); 
        $structure->setLatitude($lat); 
        $structure->setLongitude($long); 
        $structure->setUpdatedAt(new \DateTime());
        $structure->setUpdatedBy($userName);   
        
        $em->persist($structure);
        $em->flush();
      }      
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			