<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\People;
use ClassCodeBundle\Entity\Meeting;

class ImportMeetingsCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importMeetings')
    ->setDescription('Import datas from legacy classcode plugin')   
    ->addArgument('email', InputArgument::OPTIONAL, 'valid email, mandatory to use nominatim API to complete the meeting address') 
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager();
    $emp = $this->getContainer()->get('doctrine')->getManager('pixees');
    $nominatimEmail=$input->getArgument('email');
    $userName = "Import"; // pour le updatedBy
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importMeetings.log';
    $log_file_path_error = $log_dir.'/importMeetings.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import Meetings from posts ... \n";
    $old_time = time();      
    //on vide les anciennes réunions
    $old_meetings = $em->getRepository('ClassCodeBundle:Meeting')->findBy(array('updated_by' => $userName));
    foreach ($old_meetings as $m) {
      $em->remove($m);
      $em->flush(); 
    }
    
    //on recupère les posts de type rencontre et leur loc
    $rawSql = "SELECT p.*, g.* FROM wp_posts p left join wp_places_locator g on g.post_id = p.id WHERE p.post_type='rencontre' ;";
    $stmt = $emp->getConnection()->prepare($rawSql);
    $stmt->execute();
    $meetings = $stmt->fetchAll();
    
    foreach ($meetings as $m) {
      //cleaning
      if((!$m['lat']) || ($m['lat']=='NULL') || ($m['lat'] =='')){
        $m['lat'] = 0;
      }
      if((!$m['long']) || ($m['long']=='NULL') || ($m['long'] =='')){
        $m['long'] = 0;
      }

      //on recupère ses metas données
      $rawSql = "SELECT meta_key,meta_value FROM wp_postmeta WHERE post_id='".$m['ID']."' ;";
      $stmt = $emp->getConnection()->prepare($rawSql);
      $stmt->execute();
      $meetingMeta = $stmt->fetchAll();
      
      //on récupère le propriétaire
      $owner = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' => $m['post_author']));
   
      if($owner){
        $meeting = new Meeting(); 
        $meeting->setOwner($owner);
        
        foreach ($meetingMeta as $data) {
          switch($data['meta_key']){
            case "rencontre_date_1":
              $meeting->setDate($data['meta_value']);
              break;
            case "rencontre_heure_1":
              $meeting->setTime($data['meta_value']);
              break;
            case "structure":
              $meeting->setStructure($data['meta_value']);
              break;
            case "capacity":
              $meeting->setCapacity($data['meta_value']);
              break;
            case "rencontre_hangout":
              $meeting->setHangoutLink($data['meta_value']);
              break;
            case "precisions":
              $meeting->setPrecisions($data['meta_value']);
              break;
            case "rencontre_module":
              $meeting->setSubject($data['meta_value']);
              break;
            case "sujet_precisions":
              $meeting->setMore($data['meta_value']);
              break;
            case "rencontre_participants":
              //Subscribers
              $participants = explode('|', $data['meta_value']);
              foreach($participants as $participant_id){
                $participant = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' => $participant_id));
                if($participant){
                  $meeting->addSubscriber($participant);
                }
              }
              break;              
          }   
        }

  
       
        
        //recuperation des données de geoloc
        if($nominatimEmail && (($m['lat']!=0)||($m['long']!=0))){
          //on complete les données d'adresse à partir de la geoloc
          //on dort 1s pour respecter la policy https://operations.osmfoundation.org/policies/nominatim/
          sleep(1);
          $json_address = file_get_contents("https://nominatim.openstreetmap.org/reverse.php?email=".$nominatimEmail."&format=json&lat=".$m['lat']."&lon=".$m['long']);
          $json_obj = json_decode($json_address);
          if(property_exists($json_obj, "address")){
            $nominatimAddress = $json_obj->address;
            $nominatimStreet = "";
            if(property_exists($nominatimAddress, "house_number")){
              $nominatimStreet .= $nominatimAddress->house_number;  
            }
            if(property_exists($nominatimAddress, "road")){
              $nominatimStreet .= $nominatimAddress->road;  
            }   
            $nominatimCity=""; 
            if(property_exists($nominatimAddress, "village")){
              $nominatimCity .= $nominatimAddress->village;  
            }      
            $nominatimZipcode=""; 
            if(property_exists($nominatimAddress, "postcode")){
              $nominatimZipcode .= $nominatimAddress->postcode;  
            } 
            $nominatimState=""; 
            if(property_exists($nominatimAddress, "state")){
              $nominatimState .= $nominatimAddress->state;  
            } 
            $nominatimCountry=""; 
            if(property_exists($nominatimAddress, "country")){
              $nominatimCountry .= $nominatimAddress->country;  
            } 
            if($m['street'] == ''){
              $m['street']=$nominatimStreet;
            }
            if($m['city'] == ''){
              $m['city']=$nominatimCity;
            }
            if($m['state'] == ''){
              $m['state']=$nominatimState;
            }
            if($m['zipcode'] == ''){
              $m['zipcode']=$nominatimZipcode;
            }
            if($m['country_long'] == ''){
              $m['country_long']=$nominatimCountry;
            }
          }
        }
        $meeting->setStreet($m['street']);
        $meeting->setCity($m['city']);
        $meeting->setState($m['state']);
        $meeting->setZipcode($m['zipcode']);
        $meeting->setCountry($m['country_long']);
        $meeting->setFormattedAddress($m['formatted_address']);
        $meeting->setLatitude($m['lat']);
        $meeting->setLongitude($m['long']);
       
        $meeting->setUpdatedAt(new \DateTime());
        $meeting->setUpdatedBy($userName);   
        $em->persist($meeting);
        $em->flush();  
        
      }
      
                   
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			