<?php

namespace ClassCodeBundle\Command;



use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Import des utilitaires pour les opérations sur le système de fichiers
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

//Import Entity
use ClassCodeBundle\Entity\Coordination;

class ImportCoordinationsCommand extends ContainerAwareCommand
{
  protected function configure()
  {
    $this
    ->setName('classcodeadmin:importCoordinations')
    ->setDescription('Import datas from legacy classcode files')
    ->addArgument('dir', InputArgument::OPTIONAL, 'pixees root directory, default is /appli/www/pixees/')
    ;
  }
    
  protected function execute(InputInterface $input, OutputInterface $output) 
  {
    
    include 'app/Resources/lib/time_computation_lib.php';
    $em = $this->getContainer()->get('doctrine')->getManager('default');
    $userName = "Import"; // pour le updatedBy
    // desactiver les logs sql
    $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);    
 	  
  	// Path vers les logs   
    $log_dir = 'var/logs/'.$today = date("Y_m_d");
    
    if(!is_dir($log_dir)){
      mkdir($log_dir);
    }
    
    $log_file_path = $log_dir.'/importCoordinations.log';
    $log_file_path_error = $log_dir.'/importCoordinations.errors.log';    
    $log_msg = "";
    $log_msg_error = "";
        
    $log_msg .= "Import Coordinations ... \n";
    $old_time = time();   
    $root_dir = $input->getArgument('dir');
    
    if (!$root_dir){
      $root_dir="/appli/www/pixees/"; //default value (prod)
    }
    $coordinationFile=$root_dir.'wp-content/plugins/class_code_v2/structures/ClassCode_config_coordinations.php';

    if(!is_file($coordinationFile)){
      $log_msg .= "File ".$coordinationFile." not found in ".$root_dir." , aborting import !\n";
      file_put_contents($log_file_path, $log_msg);
      file_put_contents($log_file_path_error, $log_msg_error);
      return false;
    }else{
      include $coordinationFile; //$data_coordinations contain all informations now
    }
        
    foreach ($data_coordinations as $data_coordination) {
      $name = ltrim(rtrim($data_coordination['name']));  
      $classcodepage =  ltrim(rtrim($data_coordination['classCodePage']));
      $url = ltrim(rtrim($data_coordination['url']));   
      $logo = ltrim(rtrim($data_coordination['logo']));         
      $who = ltrim(rtrim($data_coordination['who']));
      $email = ltrim(rtrim($data_coordination['email']));
      $slug = ltrim(rtrim($data_coordination['slug']));
      $region = ltrim(rtrim($data_coordination['region']));
      $emails = ltrim(rtrim($data_coordination['emails']));
      $location = $data_coordination['location'];
      if($location && is_array($location)){
        $lat = ltrim(rtrim($location['lat']));
        $long = ltrim(rtrim($location['lng']));
      }else{
        $lat = 0;
        $long = 0;
      }
      $structure = ltrim(rtrim($data_coordination['structure']));
      $alert = boolval(ltrim(rtrim($data_coordination['alert'])));

      $coordination =  $em->getRepository('ClassCodeBundle:Coordination')->findOneBy(array('name' => $name));
      
      if(!$coordination){
        $coordination = new Coordination();   
        $coordination->setName($name);            
      }
      if(($coordination->getClassCodePage() != $classcodepage)||
         ($coordination->getUrl() != $url)||
         ($coordination->getLogo() != $logo)||
         ($coordination->getWho()!=$who)||
         ($coordination->getEmail() != $email)||
         ($coordination->getSlug() != $slug)||
         ($coordination->getRegion() != $region)||
         ($coordination->getEmails() != $emails)||
         ($coordination->getLatitude() != $lat)||
         ($coordination->getLongitude() != $long)||
         ($coordination->getStructure() != $structure)||
         ($coordination->getAlert() != $alert)){
        
        $coordination->setClassCodePage($classcodepage) ;
        $coordination->setUrl($url) ;
        $coordination->setLogo($logo); 
        $coordination->setWho($who);
        $coordination->setEmail($email); 
        $coordination->setSlug($slug) ;
        $coordination->setRegion($region) ;
        $coordination->setEmails($emails) ;
        $coordination->setLatitude($lat) ;
        $coordination->setLongitude($long) ;
        $coordination->setStructure($structure);
        $coordination->setAlert($alert) ;

        $coordination->setUpdatedAt(new \DateTime());
        $coordination->setUpdatedBy($userName);   
        
        $em->persist($coordination);
        $em->flush();
      }      
    }
    
    $em->clear();    
    $log_msg .= "Imports finished !\n";
    $new_time = time();  
    $log_msg .= "Execution time : ".time_elapsed($new_time-$old_time)."\n";
    $log_msg .= "Done, exiting ! \n";
    
    file_put_contents($log_file_path, $log_msg);
    file_put_contents($log_file_path_error, $log_msg_error);
    print "Execution ok ! \n";
  }
}

	    			