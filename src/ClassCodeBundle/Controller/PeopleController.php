<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\People;


class PeopleController extends Controller
{
  /**
   * @Route("/people", name="peoplepage")
   */
  public function indexAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
     
      $em = $this->getDoctrine()->getManager();  
      $people =  $em->getRepository('ClassCodeBundle:People')->findAll(); 
      $peopleWithGeoloc =  $em->getRepository('ClassCodeBundle:People')->findAllWithGeoLoc(); 
      return $this->render('@ClassCode/People/list.html.twig', array(
        'people' => $people,
        'peopleWithGeoloc' => $peopleWithGeoloc,
      ));
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig');
    } 
  }
  
   /**
   * @Route("/queryuserloc.{_format}", name="queryuserloc",defaults={"_format"="json"},requirements={"_format"="json"}))
   */
  public function queryUserLocAction(Request $request)
  {    
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
      $userLat = 0;
      $userLng = 0;
      $userStreet = "";
      $userZipCode = "";
      $userCity = "";
      $userState = "";
      $userCountry = "";
      $userFormattedAddress = "";    
      $userId = ltrim(rtrim($request->query->get("userId")));   
      if(isset($userId)&&($userId != '')){
       $em = $this->getDoctrine()->getManager();  
       $user = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$userId));
       if($user){
         $userLat = $user->getLatitude();
         $userLng = $user->getLongitude();         
         $userStreet = $user->getStreet();
         $userZipCode = $user->getZipcode();
         $userCity = $user->getCity();
         $userState = $user->getState();
         $userCountry = $user->getCountry();
         $userFormattedAddress = $user->getFormattedAddress();
       }
      }
      return $this->render('@ClassCode/People/queryUserLoc.json.twig', array(
       'userlat' => $userLat,
       'userlng' => $userLng,
       'userStreet' => $userStreet,
       'userZipcode' => $userZipCode,
       'userCity' => $userCity,
       'userCountry' => $userCountry,
       'userState' => $userState,
       'userFormattedAddress' => $userFormattedAddress
      ));  
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig');
    }
  }

  /**
   * @Route("/queryuserswithloc.{_format}", name="queryuserswithloc",defaults={"_format"="json"},requirements={"_format"="json"}))
   */
  public function queryUsersWithLocAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
      $em = $this->getDoctrine()->getManager();  
      $peopleWithGeoloc =  $em->getRepository('ClassCodeBundle:People')->findAllWithGeoLoc(); 
      $people_array = array(); 
      foreach ($peopleWithGeoloc as $people) {
        $tmp_array = array();
        $tmp_array['userId'] = $people->getUserId();
        $tmp_array['username'] = $people->getUsername();
        $tmp_array['displayname'] = $people->getCommonDisplayname();
        $tmp_array['email'] = $people->getEmail();
        $tmp_array['avatar'] = $people->getAvatar();
        $tmp_array['avatarImg'] = $people->getAvatarImg();
        $tmp_array['firstname'] = $people->getFirstname();
        $tmp_array['lastname'] = $people->getLastname();
        $tmp_array['nickname'] = $people->getNickname();
        $tmp_array['structure'] = $people->getStructure();
        $tmp_array['context'] = $people->getContext();
        $tmp_array['profile'] = $people->getProfile();
        $tmp_array['teaching'] = $people->getTeaching();
        $tmp_array['facilitator'] = $people->getFacilitator();
        $tmp_array['computerJob'] = $people->getComputerJob();
        $tmp_array['learning'] = $people->getLearning();
        $tmp_array['skills'] = $people->getSkills();
        $tmp_location_array['street'] = $people->getStreet();  
        $tmp_location_array['city'] = $people->getCity(); 
        $tmp_location_array['zipcode'] = $people->getZipcode(); 
        $tmp_location_array['state'] = $people->getState(); 
        $tmp_location_array['country'] = $people->getCountry(); 
        $tmp_location_array['formattedAddress'] = $people->getFormattedAddress(); 
        $tmp_location_array['lat'] = $people->getLatitude(); 
        $tmp_location_array['lng'] = $people->getLongitude();  
        $tmp_array['location'] = $tmp_location_array;          
        $people_array[]=$tmp_array;
      }
      return $this->render('@ClassCode/People/queryUsers.json.twig', array(
        'people' => $people_array,
      ));
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig');
    }
  }

  /**
   * @Route("/queryuserprofile.{_format}", name="queryuserprofile",defaults={"_format"="json"},requirements={"_format"="json"}))
   */
  public function queryUserProfileAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
      $people_array = array();   
      $userId = ltrim(rtrim($request->query->get("userId")));   
      if(isset($userId)&&($userId != '')){
        $em = $this->getDoctrine()->getManager();         
        $people = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$userId));
        if($people){
          $people_array['userId'] = $people->getUserId();
          $people_array['username'] = $people->getUsername();
          $people_array['displayname'] = $people->getCommonDisplayname();
          $people_array['email'] = $people->getEmail();
          $people_array['avatar'] = $people->getAvatar();
          $people_array['avatarImg'] = $people->getAvatarImg();
          $people_array['firstname'] = $people->getFirstname();
          $people_array['lastname'] = $people->getLastname();
          $people_array['nickname'] = $people->getNickname();
          $people_array['structure'] = $people->getStructure();
          $people_array['context'] = $people->getContext();
          $people_array['profile'] = $people->getProfile();
          $people_array['teaching'] = $people->getTeaching();
          $people_array['facilitator'] = $people->getFacilitator();
          $people_array['computerJob'] = $people->getComputerJob();
          $people_array['learning'] = $people->getLearning();
          $people_array['skills'] = $people->getSkills();
          $tmp_location_array['street'] = $people->getStreet();  
          $tmp_location_array['city'] = $people->getCity(); 
          $tmp_location_array['zipcode'] = $people->getZipcode(); 
          $tmp_location_array['state'] = $people->getState(); 
          $tmp_location_array['country'] = $people->getCountry(); 
          $tmp_location_array['formattedAddress'] = $people->getFormattedAddress(); 
          $tmp_location_array['lat'] = $people->getLatitude(); 
          $tmp_location_array['lng'] = $people->getLongitude(); 
          if(($tmp_location_array['lat']!='0') || ($tmp_location_array['lng']!='0')){ 
            $people_array['location'] = $tmp_location_array;
          }
        }
       }
        
      return $this->render('@ClassCode/People/queryUsers.json.twig', array(
        'people' => $people_array,
      ));
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig');
    } 
  }

 /**
   * @Route("/editpeople", name="people_edit")
   */
  public function editpeopleAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $modifiedUserId = ltrim(rtrim($request->request->get("userId")));
      $modifiedUserLogin = ltrim(rtrim($request->request->get("username")));
      $errorMessage ="";
      $action = "";
      $origin = ltrim(rtrim($request->request->get("origin"))); 
      
      if(($userName == $modifiedUserLogin) || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        if(isset($modifiedUserId)&&($modifiedUserId != '')){ 
          $people = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$modifiedUserId));
          if(!$people){
            $action = 'create' ; 
            $people = new People();
            $people->setUserId($modifiedUserId);
          }else{
            $action = "update";
          }
          
          if(null !== $request->request->get("username")){ 
            $people->setUsername(ltrim(rtrim($request->request->get("username"))));
          }
          if(null !== $request->request->get("avatar")){
            $people->setAvatar(ltrim(rtrim($request->request->get("avatar"))));
          }
          if(null !== $request->request->get("avatarImg")){
            $people->setAvatarImg(ltrim(rtrim($request->request->get("avatarImg"))));
          }
          if(null !== $request->request->get("firstname")){
            $people->setFirstname(ltrim(rtrim($request->request->get("firstname"))));
          }
          if(null !== $request->request->get("lastname")){
            $people->setLastname(ltrim(rtrim($request->request->get("lastname"))));
          }
          if(null !== $request->request->get("displayname")){ 
            $people->setDisplayname(ltrim(rtrim($request->request->get("displayname"))));
          }
          if(null !== $request->request->get("email")){
             $people->setEmail(ltrim(rtrim($request->request->get("email"))));
          }
          if(null !== $request->request->get("nickname")){
            $people->setNickname(ltrim(rtrim($request->request->get("nickname"))));
          }
          if(null !== $request->request->get("structure")){
             $people->setStructure(ltrim(rtrim($request->request->get("structure"))));
          }
          if(null !== $request->request->get("context")){
            $people->setContext(ltrim(rtrim($request->request->get("context"))));
          }
          if(null !== $request->request->get("street")){
            $people->setStreet(ltrim(rtrim($request->request->get("street"))));
          }
          if(null !== $request->request->get("city")){
            $people->setCity(ltrim(rtrim($request->request->get("city"))));
          }
          if(null !== $request->request->get("country")){
            $people->setCountry(ltrim(rtrim($request->request->get("country"))));
          }
          if(null !== $request->request->get("lat")){
            $people->setLatitude(ltrim(rtrim($request->request->get("lat"))));
          }
          if(null !== $request->request->get("lng")){
             $people->setLongitude(ltrim(rtrim($request->request->get("lng"))));
          }
          if(null !== $request->request->get("formattedAddress")){
            $people->setFormattedAddress(ltrim(rtrim($request->request->get("formattedAddress"))));
          }
          if(null !== $request->request->get("state")){
            $people->setState(ltrim(rtrim($request->request->get("state"))));
          }
          if(null !== $request->request->get("zipcode")){
            $people->setZipcode(ltrim(rtrim($request->request->get("zipcode"))));
          }
          if(null !== $request->request->get("profile")){
            $people->setProfile(ltrim(rtrim($request->request->get("profile"))));
          }
          if(null !== $request->request->get("computerJob")){
            $people->setComputerJob(ltrim(rtrim($request->request->get("computerJob"))));
          }
          if(null !== $request->request->get("teaching")){
            $people->setTeaching(ltrim(rtrim($request->request->get("teaching"))));
          }
          if(null !== $request->request->get("facilitator")){
            if($request->request->get("facilitator")=='true'){
              $people->setFacilitator(1);  
            }else{
              $people->setFacilitator(0);
            }
            
          }else{
            $people->setFacilitator(0);
          }
          if(null !== $request->request->get("learning")){
             $people->setLearning(ltrim(rtrim($request->request->get("learning"))));
          }
          if(null !== $request->request->get("skills")){
            $people->setSkills(ltrim(rtrim($request->request->get("skills"))));
          } 

          $people->setUpdatedAt(new \DateTime());
          $people->setUpdatedBy($userName);
          $em->persist($people);
          $em->flush();
          if($action == 'create'){
            $successMessage = $this->get('translator')->trans('success.peopleCreated');
          }elseif($action == 'update'){
            $successMessage = $this->get('translator')->trans('success.peopleEdited');  
          }
          
        }else{
          $errorMessage = $this->get('translator')->trans('error.peoplenotfound');
        }
                
        if($errorMessage != ''){
          $this ->get('session')->getFlashBag()->add('error', $errorMessage);  
          $result =  array('status'=>'error', 'message'=> $errorMessage);
        }else{
          $result = array('status'=>'success', 'message'=> $action);
          if($successMessage != ''){
            $this ->get('session')->getFlashBag()->add('success', $successMessage);            
          }          
        }
        if(isset($origin)&&($origin != "")){                   
          return $this->render('@ClassCode/simpleResult.json.twig', array(
           'result' => $result ,            
          ));
        }else{
          $people =  $em->getRepository('ClassCodeBundle:People')->findAll(); 
          $peopleWithGeoloc =  $em->getRepository('ClassCodeBundle:People')->findAllWithGeoLoc();          
          return $this->render('@ClassCode/People/list.html.twig', array(
            'people' => $people,
            'peopleWithGeoloc' => $peopleWithGeoloc,
          ));        
        }
      }else{
        if(isset($origin)&&($origin != "")){
          return $this->render('@ClassCode/simpleResult.json.twig', array(
            'result' => array('status'=>'error','message'=> $this->get('translator')->trans('Action non autorisée')),            
          ));
        }else{
          return  $this->render('@ClassCode/error_auth.html.twig');  
        }  
      }
    }else{
      if(isset($origin)&&($origin != "")){
        return $this->render('@ClassCode/simpleResult.json.twig', array(
         'result' => array('status'=>'error','message'=> $this->get('translator')->trans('Action non autorisée')),            
        ));
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig'); 
      }       
    }       
  }
}
