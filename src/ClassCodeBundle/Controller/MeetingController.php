<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Meeting;
use ClassCodeBundle\Entity\People;



class MeetingController extends Controller
{
  /**
   * @Route("/meetings", name="meetingspage")
   */
  public function indexAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')){
      $em = $this->getDoctrine()->getManager();       
      $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
      $meetingsWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
      return $this->render('@ClassCode/Meeting/list.html.twig', array(
        'meetings' => $meetings,
        'meetingsWithGeoloc' => $meetingsWithGeoloc,
      ));
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig');
    }
  }
  
  /**
   * @Route("/querymeetings.{_format}", name="querymeetings",defaults={"_format"="json"},requirements={"_format"="json"}))
   */
  public function queryMeetingsAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();  
    $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
    //$meetingWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
    $meetings_array = array();
    $today =  new \DateTime('NOW');
    foreach ($meetings as $m) {
      $tmp_array = array();
      $tmp_location_array = array();
      $tmp_owner = $m->getOwner();
      $tmp_facilitator = $tmp_owner->getFacilitator();
      $tmp_subsribers_array = array();
      foreach ($m->getSubscribers() as $subscriber) {
        if($subscriber->getFacilitator()){
          $tmp_facilitator = true;
        }
        $tmp_subsribers_array[]=$subscriber->getUserId();
      }
     
      $tmp_array['id'] = $m->getId();
      $tmp_array['ownerId'] = $tmp_owner->getUserId();
      $tmp_array['ownerDisplayName'] = $tmp_owner->getCommonDisplayname();
      $date = \DateTime::createFromFormat('d/m/Y', $m->getDate());
      $tmp_array['date'] = $m->getDate();
      $tmp_array["past"] = !($date != '' && $date > $today);
      $tmp_array["future"] = !($date != '' && $date < $today);
      
      $tmp_array['time'] = $m->getTime();
      $tmp_array['structure'] = $m->getStructure();
      $tmp_array['hangoutLink'] = $m->getHangoutLink();
      $tmp_array['capacity'] = $m->getCapacity();
      $tmp_array['more'] = $m->getMore();
      $tmp_array['subject'] = $m->getSubject();
      $tmp_array['precisions'] = $m->getPrecisions();
      $tmp_location_array['street'] = $m->getStreet();  
      $tmp_location_array['city'] = $m->getCity(); 
      $tmp_location_array['zipcode'] = $m->getZipcode(); 
      $tmp_location_array['state'] = $m->getState(); 
      $tmp_location_array['country'] = $m->getCountry(); 
      $tmp_location_array['formattedAddress'] = $m->getFormattedAddress(); 
      $tmp_location_array['lat'] = $m->getLatitude(); 
      $tmp_location_array['lng'] = $m->getLongitude();  
      $tmp_array['location'] = $tmp_location_array;
      $tmp_array['facilitateur'] = $tmp_facilitator;
      $tmp_array['subscribers'] = $tmp_subsribers_array;
      
      $meetings_array[]=$tmp_array;
         
    }
    return $this->render('@ClassCode/Meeting/queryMeetings.json.twig', array(
      'meetings' => $meetings_array,
    ));
  }

  /**
   * @Route("/meetinginfo.{_format}", name="meetinginfo",defaults={"_format"="json"},requirements={"_format"="json"}))
   */
  public function metingInfoAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();  
    $meetingId = ltrim(rtrim($request->query->get("meetingId")));
    $meeting = array();
    $today =  new \DateTime('NOW');
    if(isset($meetingId)&&($meetingId != '')){ 
      $m =  $em->getRepository('ClassCodeBundle:Meeting')->findOneBy(array('id' =>$meetingId));
      if($m){
        $tmp_array = array();
        $tmp_location_array = array();
        $tmp_owner = $m->getOwner();
        $tmp_facilitator = $tmp_owner->getFacilitator();
        $tmp_subsribers_array = array();
        $tmp_subsribersmails_string = $tmp_owner->getEmail();
        foreach ($m->getSubscribers() as $subscriber) {
          if($subscriber->getFacilitator()){
            $tmp_facilitator = true;
          }
          $tmp_subsribers_array[]=$subscriber->getUserId();
          $tmp_subsribersmails_string .= ";".$subscriber->getEmail();
        }
        $tmp_array['id'] = $m->getId();
        $tmp_array['ownerId'] = $tmp_owner->getUserId();
        $tmp_array['ownerDisplayName'] = $tmp_owner->getCommonDisplayname();
        $tmp_array['ownerEmail'] = $tmp_owner->getEmail();
        $date = \DateTime::createFromFormat('d/m/Y', $m->getDate());
        $tmp_array['date'] = $m->getDate();
        $tmp_array["past"] = !($date != '' && $date > $today);
        $tmp_array["future"] = !($date != '' && $date < $today);
      
        $tmp_array['time'] = $m->getTime();
        $tmp_array['structure'] = $m->getStructure();
        $tmp_array['hangoutLink'] = $m->getHangoutLink();
        $tmp_array['capacity'] = $m->getCapacity();
        $tmp_array['more'] = $m->getMore();
        $tmp_array['subject'] = $m->getSubject();
        $tmp_array['precisions'] = $m->getPrecisions();
        $tmp_location_array['street'] = $m->getStreet();  
        $tmp_location_array['city'] = $m->getCity(); 
        $tmp_location_array['zipcode'] = $m->getZipcode(); 
        $tmp_location_array['state'] = $m->getState(); 
        $tmp_location_array['country'] = $m->getCountry(); 
        $tmp_location_array['formattedAddress'] = $m->getFormattedAddress(); 
        $tmp_location_array['lat'] = $m->getLatitude(); 
        $tmp_location_array['lng'] = $m->getLongitude();  
        $tmp_array['location'] = $tmp_location_array;
        $tmp_array['facilitateur'] = $tmp_facilitator;
        $tmp_array['subscribers'] = $tmp_subsribers_array;
        $tmp_array['subscribersMails'] = $tmp_subsribersmails_string;
      
        $meeting=$tmp_array;
      }
    }
    return $this->render('@ClassCode/Meeting/meetinginfo.json.twig', array(
      'meeting' => $meeting,
    ));
  }

 /**
   * @Route("/registermeeting", name="meeting_register")
   */
  public function registermeetingAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $meetingId = ltrim(rtrim($request->request->get("meetingId")));
      $unregister = false;
      $unregister = ltrim(rtrim($request->request->get("unregister")));       
      $redirectUrl = ltrim(rtrim($request->request->get("redirect")));
      $errorMessage = "";
      $successMessage = "";
      if(isset($meetingId)&&($meetingId != '')){ 
        $m =  $em->getRepository('ClassCodeBundle:Meeting')->findOneBy(array('id' =>$meetingId));
        if($m){
          $p = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$user->getId()));
          if($p){
            if($unregister){
              $m->removeSubscriber($p);
              $em->persist($m);
              $em->flush();  
              $successMessage = $this->get('translator')->trans('success.subscriberRemoved');
            }else{
              $m->addSubscriber($p);
              $em->persist($m);
              $em->flush();  
              $successMessage = $this->get('translator')->trans('success.subscriberAdded');
            }
          }else{
            $errorMessage = $this->get('translator')->trans('error.peoplenotfound');
          }
        }else{
          $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
        }        
      }else{
        $errorMessage = $this->get('translator')->trans('error.missingparameter'); 
      }
      if(isset($redirectUrl)&&($redirectUrl != '')){
        return $this->redirect($redirectUrl);
      }else{
        if($errorMessage != ''){
          $this ->get('session')->getFlashBag()->add('error', $errorMessage);  
        }else{
          if($successMessage != ''){
            $this ->get('session')->getFlashBag()->add('success', $successMessage);
          }
        }
        $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
        $meetingsWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
        return $this->render('@ClassCode/Meeting/list.html.twig', array(
        'meetings' => $meetings,
        'meetingsWithGeoloc' => $meetingsWithGeoloc,
        ));        
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }       
  }

  /**
   * @Route("/editmeeting", name="meeting_edit")
   */
  public function editmeetingAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $redirectUrl = ltrim(rtrim($request->request->get("redirect")));
      $meetingId = ltrim(rtrim($request->request->get("meetingId")));
      $meetingDate = ltrim(rtrim($request->request->get("meeting_date")));
      $meetingTime = ltrim(rtrim($request->request->get("meeting_time")));
      $meetingStructure = ltrim(rtrim($request->request->get("structure")));
      $meetingStreet = ltrim(rtrim($request->request->get("meeting_street")));
      $meetingCity = ltrim(rtrim($request->request->get("meeting_city")));
      $meetingCountry = ltrim(rtrim($request->request->get("meeting_country")));
      $meetingCapacity = ltrim(rtrim($request->request->get("capacity")));
      $meetingHangout = ltrim(rtrim($request->request->get("meeting_hangout")));
      $meetingMore = ltrim(rtrim($request->request->get("meeting_more")));
      $meetingLat = ltrim(rtrim($request->request->get("meeting_lat"))); 
      $meetingLng = ltrim(rtrim($request->request->get("meeting_lng"))); 
      $meetingFormattedAddress = ltrim(rtrim($request->request->get("meeting_formatted_address")));  
      $meetingState = ltrim(rtrim($request->request->get("meeting_state"))); 
      $meetingZipCode = ltrim(rtrim($request->request->get("meeting_zipcode")));  
      $meetingSubject = ltrim(rtrim($request->request->get("meeting_module")));
      $meetingPrecisions = ltrim(rtrim($request->request->get("meeting_precisions")));   
      $actionDuplicate = ltrim(rtrim($request->request->get("duplicate")));
      if($actionDuplicate == 'true'){
        return $this->redirectToRoute('meeting_duplicate', array('request' => $request), 307);
      }  
      $actionDelete = ltrim(rtrim($request->request->get("delete")));                        
      if($actionDelete == 'true'){
        return $this->redirectToRoute('meeting_delete', array('request' => $request), 307);
      }
      $action = ''; 
      $p = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$user->getId()));
      if($p){           
        if(isset($meetingId)&&($meetingId != '')){ // edit 
          $entity_meeting =  $em->getRepository('ClassCodeBundle:Meeting')->findOneBy(array('id' =>$meetingId));
          if(!$entity_meeting){
            $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
          }else{
            if(($entity_meeting->getOwner()->getUserId() != $user->getId())&&(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){
              return  $this->render('@ClassCode/error_auth.html.twig'); 
            }
            $action = 'update' ;
          }        
        }else{ // create
          $action = 'create' ;
          $entity_meeting = new Meeting();
          $entity_meeting->setOwner($p); 
        }    
        $entity_meeting->setDate($meetingDate);
        $entity_meeting->setTime($meetingTime);
        $entity_meeting->setStructure($meetingStructure);
        $entity_meeting->setStreet($meetingStreet);
        $entity_meeting->setCity($meetingCity);
        $entity_meeting->setState($meetingState);
        $entity_meeting->setZipcode($meetingZipCode);
        $entity_meeting->setCountry($meetingCountry);
        $entity_meeting->setFormattedAddress($meetingFormattedAddress);
        $entity_meeting->setLatitude($meetingLat);
        $entity_meeting->setLongitude($meetingLng);
        $entity_meeting->setCapacity($meetingCapacity);
        $entity_meeting->setHangoutLink($meetingHangout);
        $entity_meeting->setMore($meetingMore);
        $entity_meeting->setSubject($meetingSubject);
        $entity_meeting->setPrecisions($meetingPrecisions);
        $entity_meeting->setUpdatedAt(new \DateTime());
        $entity_meeting->setUpdatedBy($userName);
        $em->persist($entity_meeting);
        $em->flush();
        if($action == 'create'){
          $successMessage = $this->get('translator')->trans('success.meetingCreated');
        }elseif($action == 'update'){
          $successMessage = $this->get('translator')->trans('success.meetingEdited');  
        }
        
      }else{
        $errorMessage = $this->get('translator')->trans('error.peoplenotfound');
      }
      
      if(isset($redirectUrl)&&($redirectUrl != '')){
        if(strstr($redirectUrl,"?")){
          $redirectUrl = $redirectUrl."&";
        }else{
          $redirectUrl = $redirectUrl."?";
        }
        $redirectUrl = $redirectUrl."meetingId=".$entity_meeting->getId()."&action=".$action;
        return $this->redirect($redirectUrl);
      }else{
        if($errorMessage != ''){
          $this ->get('session')->getFlashBag()->add('error', $errorMessage);  
        }else{
          if($successMessage != ''){
            $this ->get('session')->getFlashBag()->add('success', $successMessage);
          }
        }
        $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
        $meetingsWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
        return $this->render('@ClassCode/Meeting/list.html.twig', array(
        'meetings' => $meetings,
        'meetingsWithGeoloc' => $meetingsWithGeoloc,
        ));        
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }       
  }
  
  /**
   * @Route("/duplicatemeeting", name="meeting_duplicate")
   */
  public function duplicatemeetingAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $redirectUrl = ltrim(rtrim($request->request->get("redirect")));
      $meetingId = ltrim(rtrim($request->request->get("meetingId")));
                        
      $action = 'duplicate'; 
      $p = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$user->getId()));
      if($p){           
        if(isset($meetingId)&&($meetingId != '')){ // edit 
          $entity_meeting =  $em->getRepository('ClassCodeBundle:Meeting')->findOneBy(array('id' =>$meetingId));
          if(!$entity_meeting){
            $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
          }else{
            $duplicate_meeting = new Meeting();
            $duplicate_meeting->setOwner($p); 
            $duplicate_meeting->setDate($entity_meeting->getDate());
            $duplicate_meeting->setTime($entity_meeting->getTime());
            $duplicate_meeting->setStructure($entity_meeting->getStructure());
            $duplicate_meeting->setStreet($entity_meeting->getStreet());
            $duplicate_meeting->setCity($entity_meeting->getCity());
            $duplicate_meeting->setState($entity_meeting->getState());
            $duplicate_meeting->setZipcode($entity_meeting->getZipcode());
            $duplicate_meeting->setCountry($entity_meeting->getCountry());
            $duplicate_meeting->setFormattedAddress($entity_meeting->getFormattedAddress());
            $duplicate_meeting->setLatitude($entity_meeting->getLatitude());
            $duplicate_meeting->setLongitude($entity_meeting->getLongitude());
            $duplicate_meeting->setCapacity($entity_meeting->getCapacity());
            $duplicate_meeting->setHangoutLink($entity_meeting->getHangoutLink());
            $duplicate_meeting->setMore($entity_meeting->getMore());
            $duplicate_meeting->setSubject($entity_meeting->getSubject());
            $duplicate_meeting->setPrecisions($entity_meeting->getPrecisions());
            $duplicate_meeting->setUpdatedAt(new \DateTime());
            $duplicate_meeting->setUpdatedBy($userName);
            $em->persist($duplicate_meeting);
            $em->flush();
            $successMessage = $this->get('translator')->trans('success.meetingDuplicated');  
          }        
        }else{ // meeting doesn't exist
          $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
        }    
      }else{
        $errorMessage = $this->get('translator')->trans('error.peoplenotfound');
      }
      
      if(isset($redirectUrl)&&($redirectUrl != '')){
        if(strstr($redirectUrl,"?")){
          $redirectUrl = $redirectUrl."&";
        }else{
          $redirectUrl = $redirectUrl."?";
        }
        $redirectUrl = $redirectUrl."meetingId=".$duplicate_meeting->getId()."&action=".$action;
        return $this->redirect($redirectUrl);
      }else{
        if($errorMessage != ''){
          $this ->get('session')->getFlashBag()->add('error', $errorMessage);  
        }else{
          if($successMessage != ''){
            $this ->get('session')->getFlashBag()->add('success', $successMessage);
          }
        }
        $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
        $meetingsWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
        return $this->render('@ClassCode/Meeting/list.html.twig', array(
        'meetings' => $meetings,
        'meetingsWithGeoloc' => $meetingsWithGeoloc,
        ));        
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }       
  }

  /**
   * @Route("/deletemeeting", name="meeting_delete")
   */
  public function deletemeetingAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $redirectUrl = ltrim(rtrim($request->request->get("redirect")));
      $meetingId = ltrim(rtrim($request->request->get("meetingId")));
      $action = 'delete'; 
      
      $p = $em->getRepository('ClassCodeBundle:People')->findOneBy(array('userId' =>$user->getId()));
      if($p){           
        if(isset($meetingId)&&($meetingId != '')){ // delete 
          $entity_meeting =  $em->getRepository('ClassCodeBundle:Meeting')->findOneBy(array('id' =>$meetingId));
          if(!$entity_meeting){
            $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
          }else{
            if(($entity_meeting->getOwner()->getUserId() != $user->getId())&&(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){
              return  $this->render('@ClassCode/error_auth.html.twig'); 
            }else{
              $em->remove($entity_meeting);
              $em->flush();  
              $successMessage = $this->get('translator')->trans('success.meetingDeleted');     
            }
          }        
        }else{
          $errorMessage = $this->get('translator')->trans('error.meetingnotfound');
        }
      }else{
        $errorMessage = $this->get('translator')->trans('error.peoplenotfound');
      }
      
      if(isset($redirectUrl)&&($redirectUrl != '')){
        if(strstr($redirectUrl,"?")){
          $redirectUrl = $redirectUrl."&";
        }else{
          $redirectUrl = $redirectUrl."?";
        }
        $redirectUrl = $redirectUrl."action=".$action;
        return $this->redirect($redirectUrl);
      }else{
        if($errorMessage != ''){
          $this ->get('session')->getFlashBag()->add('error', $errorMessage);  
        }else{
          if($successMessage != ''){
            $this ->get('session')->getFlashBag()->add('success', $successMessage);
          }
        }
        $meetings =  $em->getRepository('ClassCodeBundle:Meeting')->findAll(); 
        $meetingsWithGeoloc =  $em->getRepository('ClassCodeBundle:Meeting')->findAllWithGeoLoc(); 
        return $this->render('@ClassCode/Meeting/list.html.twig', array(
        'meetings' => $meetings,
        'meetingsWithGeoloc' => $meetingsWithGeoloc,
        ));        
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }       
  }
}
