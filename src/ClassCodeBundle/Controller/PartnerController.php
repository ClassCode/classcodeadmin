<?php

namespace ClassCodeBundle\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Partner;
use ClassCodeBundle\Entity\Group;

class PartnerController extends Controller
{
    /**
     * @Route("/partners.{_format}", name="partnerspage",defaults={"_format"="html"},requirements={"_format"="html|json"}))
     */
    public function indexAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      $partners =  $em->getRepository('ClassCodeBundle:Partner')->findAll(); 
      $groups =  $em->getRepository('ClassCodeBundle:Group')->findAll();
      
      $format = $request->getRequestFormat();           
      
      return $this->render('@ClassCode/Partner/list.'.$format.'.twig', array(
        'partners' => $partners,
        'groups' => $groups
      ));
    }
    
    /**
     * @Route("/createpartner", name="partner_create")
     */
    public function createAction(Request $request)
    {      
      if(($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))&&($request->getMethod() == 'POST')){
        $em = $this->getDoctrine()->getManager();
        $token = $this->get('security.token_storage')->getToken() ; 
        $user = $token->getUser();
        $userName = $user->getUserName();
    
        $PartnerName = ltrim(rtrim($request->request->get("PartnerName")));   
        $PartnerLogo = ltrim(rtrim($request->request->get("PartnerLogo")));   
        $PartnerUrl = ltrim(rtrim($request->request->get("PartnerUrl")));   
        $GroupId = ltrim(rtrim($request->request->get("PartnerGroup")));   
        $group =  $em->getRepository('ClassCodeBundle:Group')->findOneBy(array('id' => $GroupId));  
          
        if(($PartnerName!='')&&($PartnerLogo!='')&&($PartnerUrl!='')&&($group)){
          $entity_partner = $em->getRepository('ClassCodeBundle:Partner')->findOneBy(
            array('name' => $PartnerName)
          );
        
          if(!$entity_partner){        
            $entity_partner = new Partner();    
            $entity_partner->setName($PartnerName); 
            $entity_partner->setLogo($PartnerLogo); 
            $entity_partner->setUrl($PartnerUrl); 
            $entity_partner->setGroup($group); 
            $entity_partner->setUpdatedAt(new \DateTime());
            $entity_partner->setUpdatedBy($userName);   
            $em->persist($entity_partner);
            $em->flush();  
            $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('partner.added'));
          }else{
            $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('partner.exist'));
          }
          return $this->redirect($this->generateUrl('partnerspage'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));
          return $this->redirect($this->generateUrl('partnerspage'));
        }
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig'); 
      }     
    }
    
    /**
     * @Route("/editpartner", name="partner_edit")
     */
    public function editAction(Request $request)
    {         
      if(($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))&&($request->getMethod() == 'POST')){
        $em = $this->getDoctrine()->getManager();
        $token = $this->get('security.token_storage')->getToken() ; 
        $user = $token->getUser();
        $userName = $user->getUserName();
        
        $PartnerId = ltrim(rtrim($request->request->get("PartnerEditId"))); 
        $PartnerName = ltrim(rtrim($request->request->get("PartnerEditName")));   
        $PartnerLogo = ltrim(rtrim($request->request->get("PartnerEditLogo")));   
        $PartnerUrl = ltrim(rtrim($request->request->get("PartnerEditUrl")));   
        $GroupId = ltrim(rtrim($request->request->get("PartnerEditGroup")));   
        
        $group =  $em->getRepository('ClassCodeBundle:Group')->findOneBy(array('id' => $GroupId));  
        $partner =  $em->getRepository('ClassCodeBundle:Partner')->findOneBy(array('id' => $PartnerId));
          
        if(($partner)&&($PartnerName!='')&&($PartnerLogo!='')&&($PartnerUrl!='')&&($group)){
          
          $entity_partner = $em->getRepository('ClassCodeBundle:Partner')
          ->createQueryBuilder('partner')
          ->select('partner')
          ->where('partner.name = :PartnerName')
          ->andwhere('partner.id != :PartnerId')
          ->setParameter('PartnerName',$PartnerName)
          ->setParameter('PartnerId',$PartnerId)
          ->getQuery()
          ->getResult();

      
         
          if(!$entity_partner){          
            $partner->setName($PartnerName); 
            $partner->setLogo($PartnerLogo); 
            $partner->setUrl($PartnerUrl); 
            $partner->setGroup($group); 
            $partner->setUpdatedAt(new \DateTime());
            $partner->setUpdatedBy($userName);   
            $em->persist($partner);
            $em->flush();  
            $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('partner.edited'));
          }else{
            $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('partner.exist'));
          }
          return $this->redirect($this->generateUrl('partnerspage'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));
          // $this ->get('session')->getFlashBag()->add('error', $GroupId);
          return $this->redirect($this->generateUrl('partnerspage'));
        }
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig'); 
      }     
    }
    
    /**
     * @Route("/deletepartner/{id}", name="partner_delete")
     * 
     */
    public function deleteAction(Request $request,$id)
    {
      $em = $this->getDoctrine()->getManager();
      $partner =  $em->getRepository('ClassCodeBundle:Partner')->find($id); 
      if(!$partner){
        throw new NotFoundHttpException();
      }else{
       if(($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))){
         $em->remove($partner);
         $em->flush();     
           $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('partner.deleted'));
        
         return $this->redirect($this->generateUrl('partnerspage'));
       }else{
         return  $this->render('@ClassCode/error_auth.html.twig');
       }
     }
   }
}
