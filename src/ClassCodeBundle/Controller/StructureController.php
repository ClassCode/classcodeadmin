<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Structure;

class StructureController extends Controller
{
  /**
   * @Route("/structures.{_format}", name="structurespage",defaults={"_format"="html"},requirements={"_format"="html|json"}))
   */
  public function indexAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    //$structures =  $em->getRepository('ClassCodeBundle:Structure')->findAll();
    $structures =  $em->getRepository('ClassCodeBundle:Structure')->findby(
      array('mainType' => 'Partenaire ClassCode')
    );
     
    $format = $request->getRequestFormat(); 
    
    return $this->render('@ClassCode/Structure/list.'.$format.'.twig', array(
      'structures' => $structures,
    ));
  }
  
  /**
   * @Route("/structuresmap.{_format}", name="structuresmappage",defaults={"_format"="html"},requirements={"_format"="html|json"}))
   */
  public function structureMapAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $type = $request->query->get('type');
    $secondaryType = $request->query->get('secondaryType');
    $queryArray = array();
    if($type){
      $queryArray['mainType'] = $type;
    }
    if($secondaryType){
      $queryArray['secondaryType'] = $secondaryType;
    }
    if(sizeof($queryArray) == 0){
      $structures =  $em->getRepository('ClassCodeBundle:Structure')->findAll();  
    }else{      
      $structures =  $em->getRepository('ClassCodeBundle:Structure')->findby(
        $queryArray
      );
    }
  
    $format = $request->getRequestFormat(); 
    
    return $this->render('@ClassCode/Structure/listAll.'.$format.'.twig', array(
      'structures' => $structures,
    ));
  }
  
  /**
   * @Route("/createstructure", name="structure_create")
   */
  public function createAction(Request $request)
  {   
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
  
      $StructureName = ltrim(rtrim($request->request->get("StructureName")));   
      $StructureType = ltrim(rtrim($request->request->get("StructureType")));   
      $StructureLogo = ltrim(rtrim($request->request->get("StructureLogo")));   
      $StructureUrl = ltrim(rtrim($request->request->get("StructureUrl")));   
      $StructureWho = ltrim(rtrim($request->request->get("StructureWho")));  
      $StructureEmail = ltrim(rtrim($request->request->get("StructureEmail")));  
      $StructureAddress = ltrim(rtrim($request->request->get("StructureAddress")));  
      $StructureLatitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("StructureLat")))));  
      $StructureLongitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("StructureLong")))));  

      if(($StructureName!='')&&($StructureLogo!='')&&($StructureUrl!='')&&
         ($StructureAddress !='')){
           
        $entity_structure = $em->getRepository('ClassCodeBundle:Structure')->findOneBy(
          array('name' => $StructureName, 'mainType'=>$StructureType)
        );
      
        if(!$entity_structure){        
          $entity_structure = new Structure();    
          $entity_structure->setName($StructureName); 
          $entity_structure->setMainType($StructureType); 
          $entity_structure->setLogo($StructureLogo); 
          $entity_structure->setUrl($StructureUrl); 
          $entity_structure->setWho($StructureWho); 
          $entity_structure->setEmail($StructureEmail); 
          $entity_structure->setAddress($StructureAddress); 
          $entity_structure->setLatitude($StructureLatitude); 
          $entity_structure->setLongitude($StructureLongitude); 
          $entity_structure->setDataSource("web"); 
          $entity_structure->setUpdatedAt(new \DateTime());
          $entity_structure->setUpdatedBy($userName);   
          $em->persist($entity_structure);
          $em->flush();  
          $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('structure.added'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('structure.exist'));
        }
        return $this->redirect($this->generateUrl('structurespage'));
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));

        return $this->redirect($this->generateUrl('structurespage'));
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }     
  }
  
  /**
   * @Route("/editstructure", name="structure_edit")
   */
  public function editAction(Request $request)
  {    
    if($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
  
      $StructureId = ltrim(rtrim($request->request->get("StructureEditId"))); 
      $StructureName = ltrim(rtrim($request->request->get("StructureEditName")));  
      $StructureType = ltrim(rtrim($request->request->get("StructureEditType")));    
      $StructureLogo = ltrim(rtrim($request->request->get("StructureEditLogo")));   
      $StructureUrl = ltrim(rtrim($request->request->get("StructureEditUrl")));   
      $StructureWho = ltrim(rtrim($request->request->get("StructureEditWho")));  
      $StructureEmail = ltrim(rtrim($request->request->get("StructureEditEmail")));  
      $StructureAddress = ltrim(rtrim($request->request->get("StructureEditAddress")));  
      $StructureLatitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("StructureEditLat")))));  
      $StructureLongitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("StructureEditLong")))));      
      
      $structure =  $em->getRepository('ClassCodeBundle:Structure')->findOneBy(array('id' => $StructureId));
      
      if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')||
         ($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($userName=$structure->getUpdatedBy()))){
        
        if(($structure)&&($StructureName!='')&&($StructureLogo!='')&&($StructureUrl!='')&&
           ($StructureAddress !='')){
  
          $entity_structure = $em->getRepository('ClassCodeBundle:Structure')
          ->createQueryBuilder('structure')
          ->select('structure')
          ->where('structure.name = :StructureName')
          ->andwhere('structure.mainType = :StructureType')
          ->andwhere('structure.id != :StructureId')
          ->setParameter('StructureName',$StructureName)
          ->setParameter('StructureType',$StructureType)
          ->setParameter('StructureId',$StructureId)
          ->getQuery()
          ->getResult();
  
          if(!$entity_structure){           
            $structure->setName($StructureName); 
            $structure->setMainType($StructureType); 
            $structure->setLogo($StructureLogo); 
            $structure->setUrl($StructureUrl); 
            $structure->setWho($StructureWho); 
            $structure->setEmail($StructureEmail); 
            $structure->setAddress($StructureAddress); 
            $structure->setLatitude($StructureLatitude); 
            $structure->setLongitude($StructureLongitude); 
            $structure->setDataSource("web"); 
            $structure->setUpdatedAt(new \DateTime());
            $structure->setUpdatedBy($userName);   
            $em->persist($structure);
            $em->flush();  
            $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('structure.edited'));
          }else{
            $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('structure.exist'));
          }
          return $this->redirect($this->generateUrl('structurespage'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));          
        }
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('access_error'));
      }
      return $this->redirect($this->generateUrl('structurespage'));
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }     
  }
  
  /**
   * @Route("/deletestructure/{id}", name="structure_delete")
   * 
   */
  public function deleteAction(Request $request,$id)
  {
    $em = $this->getDoctrine()->getManager();
    $token = $this->get('security.token_storage')->getToken() ; 
    $user = $token->getUser();
    $userName = $user->getUserName();
    $structure =  $em->getRepository('ClassCodeBundle:Structure')->find($id); 
    if(!$structure){
      throw new NotFoundHttpException();
    }else{
      if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')||
         ($this->get('security.authorization_checker')->isGranted('ROLE_USER')&&($userName=$structure->getUpdatedBy()))){
        $em->remove($structure);
        $em->flush();     
        $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('structure.deleted'));      
        return $this->redirect($this->generateUrl('structurespage'));
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('access_error'));
        return  $this->render('@ClassCode/error_auth.html.twig');
      }
    }
  }
  
  /**
   * @Route("/shufflestructureslogos", name="structure_shuffle")
   * 
   */
  public function shuffleLogosAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $structures =  $em->getRepository('ClassCodeBundle:Structure')->findAll(); 
    $structuresLogos = array();
    foreach ($structures as $structure) {
      $logo = $structure->getLogo();
      if($logo && !in_array($logo,$structuresLogos)){
        $structuresLogos[] = $logo;        
      }  
    }
    shuffle($structuresLogos);
    return $this->render('@ClassCode/Structure/shufflelogos.html.twig', array(
      'structuresLogos' => $structuresLogos,
    ));
  }
}
