<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Coordination;

class CoordinationController extends Controller
{
  /**
   * @Route("/coordinations.{_format}", name="coordinationspage",defaults={"_format"="html"},requirements={"_format"="html|json"}))
   */
  public function indexAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $coordinations =  $em->getRepository('ClassCodeBundle:Coordination')->findAll(); 
    $format = $request->getRequestFormat(); 
     
    return $this->render('@ClassCode/Coordination/list.'.$format.'.twig', array(
      'coordinations' => $coordinations,
    ));
  }
  
  /**
   * @Route("/createcoordination", name="coordination_create")
   */
  public function createAction(Request $request)
  {
    if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $CoordinationName = ltrim(rtrim($request->request->get("CoordinationName")));
      $CoordinationClassCodePage = ltrim(rtrim($request->request->get("CoordinationClassCodePage")));      
      $CoordinationLogo = ltrim(rtrim($request->request->get("CoordinationLogo")));   
      $CoordinationUrl = ltrim(rtrim($request->request->get("CoordinationUrl")));   
      $CoordinationWho = ltrim(rtrim($request->request->get("CoordinationWho")));  
      $CoordinationEmail = ltrim(rtrim($request->request->get("CoordinationEmail"))); 
      
      $CoordinationEmails = ltrim(rtrim($request->request->get("CoordinationEmails")));  
      $CoordinationRegion = ltrim(rtrim($request->request->get("CoordinationRegion")));
      $CoordinationSlug = ltrim(rtrim($request->request->get("CoordinationSlug")));   
      $CoordinationStructure = ltrim(rtrim($request->request->get("CoordinationStructure")));        
      $CoordinationAlert = filter_var($request->request->get("CoordinationAlert"), FILTER_VALIDATE_BOOLEAN); 
      
      $CoordinationAddress = ltrim(rtrim($request->request->get("CoordinationAddress")));  
      $CoordinationLatitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("CoordinationLat")))));  
      $CoordinationLongitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("CoordinationLong")))));  
 
        if(($CoordinationName!='')&&($CoordinationLogo!='')&&($CoordinationUrl!='')){
           
        $entity_coordination = $em->getRepository('ClassCodeBundle:Coordination')->findOneBy(
          array('name' => $CoordinationName)
        );
      
        if(!$entity_coordination){        
          $entity_coordination = new Coordination();    
          $entity_coordination->setName($CoordinationName); 
          $entity_coordination->setClassCodePage($CoordinationClassCodePage); 
          $entity_coordination->setLogo($CoordinationLogo); 
          $entity_coordination->setUrl($CoordinationUrl); 
          $entity_coordination->setWho($CoordinationWho); 
          $entity_coordination->setEmail($CoordinationEmail); 
          
          $entity_coordination->setEmails($CoordinationEmails); 
          $entity_coordination->setRegion($CoordinationRegion);
          $entity_coordination->setSlug($CoordinationSlug);
          $entity_coordination->setStructure($CoordinationStructure);
          $entity_coordination->setAlert($CoordinationAlert);
          
          $entity_coordination->setAddress($CoordinationAddress);
          $entity_coordination->setLatitude($CoordinationLatitude); 
          $entity_coordination->setLongitude($CoordinationLongitude); 

          $entity_coordination->setUpdatedAt(new \DateTime());
          $entity_coordination->setUpdatedBy($userName);   
          $em->persist($entity_coordination);
          $em->flush();  
          $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('coordination.added'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('coordination.exist'));
        }
        return $this->redirect($this->generateUrl('coordinationspage'));
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));
        return $this->redirect($this->generateUrl('coordinationspage'));
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }     
  }
  
  /**
   * @Route("/editcoordination", name="coordination_edit")
   */
  public function editAction(Request $request)
  {    
    if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $CoordinationId = ltrim(rtrim($request->request->get("CoordinationEditId"))); 
      $CoordinationName = ltrim(rtrim($request->request->get("CoordinationEditName")));
      $CoordinationClassCodePage = ltrim(rtrim($request->request->get("CoordinationEditClassCodePage")));         
      $CoordinationLogo = ltrim(rtrim($request->request->get("CoordinationEditLogo")));   
      $CoordinationUrl = ltrim(rtrim($request->request->get("CoordinationEditUrl")));   
      $CoordinationWho = ltrim(rtrim($request->request->get("CoordinationEditWho")));  
      $CoordinationEmail = ltrim(rtrim($request->request->get("CoordinationEditEmail")));  
      
      $CoordinationEmails = ltrim(rtrim($request->request->get("CoordinationEditEmails")));  
      $CoordinationRegion = ltrim(rtrim($request->request->get("CoordinationEditRegion")));
      $CoordinationSlug = ltrim(rtrim($request->request->get("CoordinationEditSlug")));   
      $CoordinationStructure = ltrim(rtrim($request->request->get("CoordinationEditStructure")));
      $CoordinationAlert = filter_var($request->request->get("CoordinationEditAlert"), FILTER_VALIDATE_BOOLEAN); 
      
      $CoordinationAddress = ltrim(rtrim($request->request->get("CoordinationEditAddress")));  
      $CoordinationLatitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("CoordinationEditLat")))));  
      $CoordinationLongitude = floatval(str_replace(',','.', ltrim(rtrim($request->request->get("CoordinationEditLong")))));      
      
      
      $coordination =  $em->getRepository('ClassCodeBundle:Coordination')->findOneBy(array('id' => $CoordinationId));
        
      if(($coordination)&&($CoordinationName!='')&&($CoordinationLogo!='')&&($CoordinationUrl!='')&&
         ($CoordinationRegion !='')){
           
        
        $entity_coordination = $em->getRepository('ClassCodeBundle:Coordination')
        ->createQueryBuilder('coordination')
        ->select('coordination')
        ->where('coordination.name = :CoordinationName')
        ->andwhere('coordination.id != :CoordinationId')
        ->setParameter('CoordinationName',$CoordinationName)
        ->setParameter('CoordinationId',$CoordinationId)
        ->getQuery()
        ->getResult();
        
        if(!$entity_coordination){           
          $coordination->setName($CoordinationName); 
          $coordination->setClassCodePage($CoordinationClassCodePage); 
          $coordination->setLogo($CoordinationLogo); 
          $coordination->setUrl($CoordinationUrl); 
          $coordination->setWho($CoordinationWho); 
          $coordination->setEmail($CoordinationEmail); 
          
          $coordination->setEmails($CoordinationEmails); 
          $coordination->setRegion($CoordinationRegion);
          $coordination->setSlug($CoordinationSlug);
          $coordination->setStructure($CoordinationStructure);
          $coordination->setAlert($CoordinationAlert);
          
          $coordination->setAddress($CoordinationAddress);
          $coordination->setLatitude($CoordinationLatitude); 
          $coordination->setLongitude($CoordinationLongitude); 

          $coordination->setUpdatedAt(new \DateTime());
          $coordination->setUpdatedBy($userName);   
          $em->persist($coordination);
          $em->flush();  
          $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('coordination.edited'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('coordination.exist'));
        }
        return $this->redirect($this->generateUrl('coordinationspage'));
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));
        return $this->redirect($this->generateUrl('coordinationspage'));
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }     
  }
  
  /**
   * @Route("/deletecoordination/{id}", name="coordination_delete")
   * 
   */
  public function deleteAction(Request $request,$id)
  {
    $em = $this->getDoctrine()->getManager();
    $coordination =  $em->getRepository('ClassCodeBundle:Coordination')->find($id); 
    if(!$coordination){
      throw new NotFoundHttpException();
    }else{
      if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
        $em->remove($coordination);
        $em->flush();     
        $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('coordination.deleted'));        
        return $this->redirect($this->generateUrl('coordinationspage'));
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig');
      }
    }
  }
}
