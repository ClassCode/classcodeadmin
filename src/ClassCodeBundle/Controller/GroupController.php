<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Partner;
use ClassCodeBundle\Entity\Group;

class GroupController extends Controller
{
  /**
   * @Route("/groups.{_format}", name="groupspage", defaults={"_format"="html"},requirements={"_format"="html|json"}))
   */
  public function indexAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $groups =  $em->getRepository('ClassCodeBundle:Group')->findAll(); 
    $format = $request->getRequestFormat();   
     
    return $this->render('@ClassCode/Group/list.'.$format.'.twig', array(
      'groups' => $groups
    ));
  }
  
  /**
   * @Route("/creategroup", name="group_create")
   */
  public function createAction(Request $request)
  {    
    if(($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))&&($request->getMethod() == 'POST')){
      $em = $this->getDoctrine()->getManager();  
      $token = $this->get('security.token_storage')->getToken() ; 
      $user = $token->getUser();
      $userName = $user->getUserName();
      $GroupName = ltrim(rtrim($request->request->get("GroupName")));        
      if($GroupName!=''){
        $entity_group = $em->getRepository('ClassCodeBundle:Group')->findOneBy(
          array('name' => $GroupName)
        );
      
        if(!$entity_group){        
          $entity_group = new Group();    
          $entity_group->setName($GroupName); 
          $entity_group->setUpdatedAt(new \DateTime());
          $entity_group->setUpdatedBy($userName);   
          $em->persist($entity_group);
          $em->flush();  
          $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('group.added'));
        }else{
          $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('group.exist'));
        }
        return $this->redirect($this->generateUrl('groupspage'));
      }else{
        $this ->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.errors'));
        return $this->redirect($this->generateUrl('groupspage'));
      }
    }else{
      return  $this->render('@ClassCode/error_auth.html.twig'); 
    }     
  }
  
  /**
   * @Route("/updategroup", name="group_update")
   */
  public function update_attributeAction(Request $request)
  {
    if($request->isXmlHttpRequest()) { 
      $em = $this->getDoctrine()->getManager();
      $token = $this->get('security.token_storage')->getToken() ;
      $user = $token->getUser();
      $userName = $user->getUserName();
      // recupere le "data-pk" qui correspond à l'id        
      $id = $request->request->get('pk');
      $group = $em->getRepository('ClassCodeBundle:Group')->findOneBy(array('id' => $id));
      //champ a modifier
      $name = rtrim($request->request->get('name'));
     //valeur
      $value = rtrim($request->request->get('value'));
      
      if( ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))&&($group)&&($name=="name")&&($value!='')){
        //On cherche si un groupe n'existe pas déjà avec ce nom
        $group_exist = $em->getRepository('ClassCodeBundle:Group')->findOneBy(array('name' => $value));
        if($group_exist){
          // on renvoit une erreur
          throw new NotFoundHttpException();
        }else{
          $group->setName($value);
          $group->setUpdatedAt(new \DateTime());
          $group->setUpdatedBy($userName);   
          $em->persist($group);
          $em->flush();
          
          $groups =  $em->getRepository('ClassCodeBundle:Group')->findAll();     
          return $this->render('@ClassCode/Group/ajax_list.html.twig', array('groups' => $groups));            
        }
        
  
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig');
      }
    } else {
      // Si ce n'est pas une requête AJAX
      // On met une erreur (juste au cas où) de type HTTP 404
      throw new NotFoundHttpException();
    }        
  }
  
   
  /**
   * @Route("/deletegroup/{id}", name="group_delete")
   * 
   */
  public function deleteAction(Request $request,$id)
  {
    $em = $this->getDoctrine()->getManager();
    $group =  $em->getRepository('ClassCodeBundle:Group')->find($id); 
    if(!$group){
      throw new NotFoundHttpException();
    }else{
      if($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){ 
        $em->remove($group);
        $em->flush();     
        $this ->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('group.deleted'));  
        return $this->redirect($this->generateUrl('groupspage'));
      }else{
        return  $this->render('@ClassCode/error_auth.html.twig');
      }
    }
  }
}
