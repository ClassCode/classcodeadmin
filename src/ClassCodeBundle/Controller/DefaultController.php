<?php

namespace ClassCodeBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use ClassCodeBundle\Entity\Partner;
use ClassCodeBundle\Entity\Group;

class DefaultController extends Controller
{
  /**
   * @Route("/", name="indexpage")
   */
  public function indexAction(Request $request)
  {       
    return $this->render('@ClassCode/index.html.twig');
  }
    
  /**
   * @Route("/logout", name="logout")
   */
  public function logoutAction(Request $request)
  {
    //redirection to the wp logout page         
    return $this->redirect($request->getSchemeAndHttpHost()."/wp-login.php?action=logout");       
  }
  
  /**
   * @Route("/login", name="login")
   */
  public function loginAction(Request $request)
  {
    //redirection to the wp login page         
    return $this->redirect($request->getSchemeAndHttpHost()."/wp-login.php?action=login");       
  }
}
